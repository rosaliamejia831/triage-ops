# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/lazy_heat_map'

RSpec.describe LazyHeatMap do
  subject { described_class.new(resources, policy_spec, mock_network) }

  let(:options) { Struct.new(:source, :source_id, :host_url, :api_version) }

  let(:host_url) { 'https://gitlab.com' }
  let(:resource_path) { 'g/p' }
  let(:network_options) do
    options.new(:projects, resource_path)
  end

  let(:mock_network) { double(options: network_options) }
  let(:resource_base) do
    {
      type: 'issues',
      web_url: 'https://gitlab.com/g/p/-/issues/1',
    }
  end

  let(:resources) do
    [
      resource_base.merge(labels: %w[priority::1 severity::2]),
      resource_base.merge(labels: %w[priority::3 severity::3]),
      resource_base.merge(labels: %w[priority::2]),
      resource_base.merge(labels: %w[severity::3]),
      resource_base.merge(labels: %w[])
    ]
  end

  let(:policy_spec) do
    { conditions: { state: 'open', labels: %w[bug Category] } }
  end

  before do
    allow(mock_network).to receive(:query_api_cached).and_return(
      [
        { web_url: "#{host_url}/#{resource_path}" }
      ]
    )
  end

  describe '#to_s' do
    it 'generates the heatmap' do
      p1s2 = '[1](https://gitlab.com/g/p/-/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=priority%3A%3A1&label_name%5B%5D=severity%3A%3A2)'
      p3s3 = '[1](https://gitlab.com/g/p/-/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=priority%3A%3A3&label_name%5B%5D=severity%3A%3A3)'
      p2 = '[1](https://gitlab.com/g/p/-/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=priority%3A%3A2&not%5Blabel_name%5D%5B%5D=severity%3A%3A1&not%5Blabel_name%5D%5B%5D=severity%3A%3A2&not%5Blabel_name%5D%5B%5D=severity%3A%3A3&not%5Blabel_name%5D%5B%5D=severity%3A%3A4)'
      s3 = '[1](https://gitlab.com/g/p/-/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=severity%3A%3A3&not%5Blabel_name%5D%5B%5D=priority%3A%3A1&not%5Blabel_name%5D%5B%5D=priority%3A%3A2&not%5Blabel_name%5D%5B%5D=priority%3A%3A3&not%5Blabel_name%5D%5B%5D=priority%3A%3A4)'
      neither = '[1](https://gitlab.com/g/p/-/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&not%5Blabel_name%5D%5B%5D=priority%3A%3A1&not%5Blabel_name%5D%5B%5D=priority%3A%3A2&not%5Blabel_name%5D%5B%5D=priority%3A%3A3&not%5Blabel_name%5D%5B%5D=priority%3A%3A4&not%5Blabel_name%5D%5B%5D=severity%3A%3A1&not%5Blabel_name%5D%5B%5D=severity%3A%3A2&not%5Blabel_name%5D%5B%5D=severity%3A%3A3&not%5Blabel_name%5D%5B%5D=severity%3A%3A4)'

      expect(subject.to_s).to eq(<<~MARKDOWN.chomp)
        || ~"severity::1" | ~"severity::2" | ~"severity::3" | ~"severity::4" | No severity |
        |----|----|----|----|----|----|
        | ~"priority::1" | 0 | #{p1s2} | 0 | 0 | 0 |
        | ~"priority::2" | 0 | 0 | 0 | 0 | #{p2} |
        | ~"priority::3" | 0 | 0 | #{p3s3} | 0 | 0 |
        | ~"priority::4" | 0 | 0 | 0 | 0 | 0 |
        | No priority | 0 | 0 | #{s3} | 0 | #{neither} |
      MARKDOWN
    end

    context 'when all of resources have priorities and severities' do
      let(:resources) do
        [
          resource_base.merge(labels: %w[priority::1 severity::4]),
          resource_base.merge(labels: %w[priority::1 severity::4]),
          resource_base.merge(labels: %w[priority::1 severity::4]),
          resource_base.merge(labels: %w[priority::1 severity::4]),
          resource_base.merge(labels: %w[priority::1 severity::4])
        ]
      end

      it 'still shows No priorities/severities columns' do
        p1s4 = '[5](https://gitlab.com/g/p/-/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=priority%3A%3A1&label_name%5B%5D=severity%3A%3A4)'

        expect(subject.to_s).to eq(<<~MARKDOWN.chomp)
          || ~"severity::1" | ~"severity::2" | ~"severity::3" | ~"severity::4" | No severity |
          |----|----|----|----|----|----|
          | ~"priority::1" | 0 | 0 | 0 | #{p1s4} | 0 |
          | ~"priority::2" | 0 | 0 | 0 | 0 | 0 |
          | ~"priority::3" | 0 | 0 | 0 | 0 | 0 |
          | ~"priority::4" | 0 | 0 | 0 | 0 | 0 |
          | No priority | 0 | 0 | 0 | 0 | 0 |
        MARKDOWN
      end
    end

    context 'for a project' do
      it 'url contains project path' do
        expect(subject.to_s).to include("gitlab.com/g/p/-/issues")
      end

      context 'for a numeric project ID' do
        let(:project_id) { 123 }
        let(:network_options) do
          options.new(:projects, project_id)
        end

        it 'url contains project path' do
          expect(subject.to_s).to include("gitlab.com/g/p/-/issues")
        end
      end

      context 'for a subgroup project' do
        let(:resource_path) { 'group/subgroup/project' }

        it 'url contains project path' do
          expect(subject.to_s).to include("gitlab.com/group/subgroup/project/-/issues")
        end
      end
    end

    context 'for a group' do
      before do
        allow(mock_network).to receive(:query_api_cached).and_return(
          [
            { web_url: "#{host_url}/groups/#{resource_path}" }
          ]
        )
      end

      let(:resource_path) { 'g' }
      let(:network_options) do
        options.new(:groups, resource_path)
      end

      it 'url contains group path' do
        expect(subject.to_s).to include("gitlab.com/groups/g/-/issues")
      end

      context 'for a numeric group ID' do
        let(:group_id) { 123 }
        let(:network_options) do
          options.new(:groups, group_id)
        end

        it 'url contains group path' do
          expect(subject.to_s).to include("gitlab.com/groups/g/-/issues")
        end
      end

      context 'for a subgroup group' do
        let(:resource_path) { 'group/subgroup' }

        it 'url contains project path' do
          expect(subject.to_s).to include("gitlab.com/groups/group/subgroup/-/issues")
        end
      end
    end
  end
end
