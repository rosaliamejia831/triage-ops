# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/www_gitlab_com'
require_relative '../../lib/merge_request_coach_helper'

RSpec.describe MergeRequestCoachHelper do
  let(:resource_klass) do
    Class.new do
      include MergeRequestCoachHelper
    end
  end

  let(:roulette) do
    [
      { 'username' => 'first_coach', 'merge_request_coach' => true, 'available' => true },
      { 'username' => 'second_coach', 'merge_request_coach' => true, 'available' => true },
      { 'username' => 'unavailable_coach', 'merge_request_coach' => true, 'available' => false },
      { 'username' => 'not_coach', 'merge_request_coach' => false, 'available' => true }
    ]
  end

  before do
    allow(WwwGitLabCom).to receive(:roulette).and_return(roulette)
  end

  describe '#select_random_merge_request_coach' do
    subject(:select_random_merge_request_coach) { resource_klass.new.select_random_merge_request_coach }

    it 'returns random mr coach' do
      expect(select_random_merge_request_coach).to eq('@first_coach').or(eq('@second_coach'))
    end
  end
end
