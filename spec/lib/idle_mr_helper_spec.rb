# frozen_string_literal: true

require 'spec_helper'

require_relative '../../lib/idle_mr_helper'
require_relative '../../lib/stale_resources_helper'

RSpec.describe IdleMrHelper do
  describe '.idle?' do
    subject(:idle) { described_class.idle?(resource) }

    let(:resource) { { title: title } }
    let(:title) { 'Sample' }

    before do
      allow(StaleResources).to receive_message_chain(:new, :days_since_last_human_update).and_return(days_since_last_human_update)
    end

    context 'when MR is not stale' do
      let(:days_since_last_human_update) { 1 }

      it { is_expected.to eq(false) }
    end

    context 'when MR is stale' do
      let(:days_since_last_human_update) { 50 }

      context 'when MR is wip' do
        let(:title) { 'Draft: Sample' }

        it { is_expected.to eq(false) }
      end

      it { is_expected.to eq(true) }
    end
  end

  describe '.days_since_last_human_update' do
    subject(:days_since_last_human_update) { described_class.days_since_last_human_update({}) }

    before do
      allow(StaleResources).to receive_message_chain(:new, :days_since_last_human_update).and_return(10)
    end

    it { is_expected.to eq(10) }
  end
end
