# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/processor'
require_relative '../../triage/triage/related_issue_finder'

RSpec.describe Triage::RelatedIssueFinder do
  let(:project_id) { 12 }
  let(:text) do
    <<~TEXT
    Lorem ipsum

    #{related_issue_line}
    TEXT
  end

  context 'with the default regex' do
    let(:iid) { "42" }

    context 'with a related issue' do
      describe '#find_issue_in(text)' do
        where(:related_issue_line) do
          [
            "Related to ##{iid}",
            "Related to ##{iid}.",
            "related to ##{iid}",
            "related to ##{iid}.",
            "related to ##{iid}, #43.",
          ]
        end

        with_them do
          it "finds an issue" do
            issue = subject.find_issue_in(text)

            expect(issue.iid).to eq(iid)
          end
        end
      end

      describe '#find_project_issue_in(project_id, text)' do
        where(:related_issue_line) do
          [
            "Related to ##{iid}",
            "Related to ##{iid}.",
            "related to ##{iid}",
            "related to ##{iid}.",
            "related to ##{iid}, #43.",
          ]
        end

        with_them do
          it "finds an issue" do
            issue = subject.find_project_issue_in(project_id, text)

            expect(issue.project_id).to eq(project_id)
            expect(issue.iid).to eq(iid)
          end
        end
      end
    end

    context 'with no related issue' do
      let(:related_issue_line) { '' }

      describe '#find_issue_in(text)' do
        it "does not find an issue" do
          issue = subject.find_issue_in(text)

          expect(issue).to be_nil
        end
      end

      describe '#find_project_issue_in(project_id, text)' do
        it "does not find an issue" do
          issue = subject.find_project_issue_in(project_id, text)

          expect(issue).to be_nil
        end
      end
    end
  end


  context 'with custom regex' do
    let(:iid) { "42" }

    subject { described_class.new(related_issue_regex: /see #(?<iid>\d+)/i) }

    context 'with a related issue' do
      describe '#find_issue_in(text)' do
        where(:related_issue_line) do
          [
            "see ##{iid}",
            "see ##{iid}.",
            "see ##{iid}",
            "see ##{iid}.",
            "see ##{iid}, #43.",
          ]
        end

        with_them do
          it "finds an issue" do
            issue = subject.find_issue_in(text)

            expect(issue.iid).to eq(iid)
          end
        end
      end

      describe '#find_project_issue_in(project_id, text)' do
        where(:related_issue_line) do
          [
            "see ##{iid}",
            "see ##{iid}.",
            "see ##{iid}",
            "see ##{iid}.",
            "see ##{iid}, #43.",
          ]
        end

        with_them do
          it "finds an issue" do
            issue = subject.find_project_issue_in(project_id, text)

            expect(issue.project_id).to eq(project_id)
            expect(issue.iid).to eq(iid)
          end
        end
      end
    end

    context 'with no related issue' do
      let(:related_issue_line) { '' }

      describe '#find_issue_in(text)' do
        it "does not find an issue" do
          issue = subject.find_issue_in(text)

          expect(issue).to be_nil
        end
      end

      describe '#find_project_issue_in(project_id, text)' do
        it "does not find an issue" do
          issue = subject.find_project_issue_in(project_id, text)

          expect(issue).to be_nil
        end
      end
    end
  end

  describe Triage::RelatedIssueFinder::Issue do
    let(:iid) { "42" }

    subject { described_class.new(project_id: project_id, iid: iid) }

    it { expect(subject.project_id).to eq(project_id) }
    it { expect(subject.iid).to eq(iid) }

    describe '#labels' do
      let(:labels) { %w[foo bar] }
      let(:issue_attrs) { { 'labels' => labels } }

      it 'returns an array of label names' do
        expect_api_request(path: "/projects/#{subject.project_id}/issues/#{subject.iid}", response_body: issue_attrs) do
          expect(subject.labels).to match_array(labels)
        end
      end

      it 'returns an empty array if issue is not found' do
        stub_api_request(path: "/projects/#{project_id}/issues/#{iid}", response_status: 404, response_body: { message: "404 Not found" })

        expect(subject.labels).to eq([])
      end
    end
  end
end
