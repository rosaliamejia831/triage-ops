# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/event'

RSpec.describe Triage::Event do
  subject { described_class.build(event) }

  let(:fixture_path) { 'note_on_issue.json' }
  let(:event) do
    JSON.parse(File.read("#{__dir__}/../fixtures/reactive/#{fixture_path}"))
  end
  let(:labels) { event['labels'].map { |label| label['title'] } }
  let(:current_labels) { event['changes']['labels']['current'].map { |label| label['title'] } }
  let(:previous_labels) { event['changes']['labels']['previous'].map { |label| label['title'] } }

  describe '.build' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns a IssueEvent' do
        expect(subject).to be_a(Triage::IssueEvent)
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns a IssueEvent' do
        expect(subject).to be_a(Triage::MergeRequestEvent)
      end
    end

    context 'when the event is a note on an issue' do
      it 'returns a IssueEvent' do
        expect(subject).to be_a(Triage::NoteEvent)
      end
    end

    context 'when the event is a note on a merge request' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      it 'returns a IssueEvent' do
        expect(subject).to be_a(Triage::NoteEvent)
      end
    end

    context 'when object_kind is not provided' do
      let(:event) { {} }

      it 'raises ObjectKindNotProvidedError' do
        expect { subject }.to raise_error(described_class::ObjectKindNotProvidedError)
      end
    end

    context 'when object_kind is unknown' do
      let(:event) { { 'object_kind' => 'unknown' } }

      it 'raises UnknownObjectKind' do
        expect { subject }.to raise_error(described_class::UnknownObjectKind)
      end
    end
  end

  describe '#key' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns issue.open' do
        expect(subject.key).to eq("issue.open")
      end
    end
  end

  describe '#issue?' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns true' do
        expect(subject).to be_issue
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      it 'returns false' do
        expect(subject).not_to be_issue
      end
    end
  end

  describe '#merge_request?' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns false' do
        expect(subject).not_to be_merge_request
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns true' do
        expect(subject).to be_merge_request
      end
    end
  end

  describe '#note?' do
    context 'when the event is not a note' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns false' do
        expect(subject).not_to be_note
      end
    end

    context 'when the event is a new note on an issue' do
      it 'returns true' do
        expect(subject).to be_note
      end
    end

    context 'when the event is a new note on a MR' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      it 'returns true' do
        expect(subject).to be_note
      end
    end
  end

  describe '#event_actor' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns a User object' do
        expect(subject.event_actor).to be_a(Triage::User)
        expect(subject.event_actor.to_h).to eq(event['user'])
        expect(subject.event_actor['username']).to eq(event['user']['username'])
      end
    end
  end

  describe '#event_actor_id' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns object_attributes.user.id' do
        expect(subject.event_actor_id).to eq(event['user']['id'])
      end
    end
  end

  describe '#event_actor_username' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns object_attributes.user.username' do
        expect(subject.event_actor_username).to eq(event['user']['username'])
      end
    end
  end

  describe '#resource_author_id' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns object_attributes.author_id' do
        expect(subject.resource_author_id).to eq(event['object_attributes']['author_id'])
      end
    end
  end

  describe '#resource_author' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }
      let(:user_attrs) { { 'id' => event['object_attributes']['author_id'], 'username' => 'root' } }

      it 'returns a User object that corresponds to the object_attributes.author_id' do
        expect_api_request(path: "/users/#{user_attrs['id']}", response_body: user_attrs) do
          expect(subject.resource_author).to be_a(Triage::User)
          expect(subject.resource_author.to_h).to eq(user_attrs)
          expect(subject.resource_author.id).to eq(user_attrs['id'])
          expect(subject.resource_author.username).to eq(user_attrs['username'])
        end
      end
    end
  end

  describe '#created_at' do
    it 'returns the resource created_at time' do
      expect(subject.created_at).to eq(Time.parse(event['object_attributes']['created_at']))
    end
  end

  describe '#url' do
    let(:fixture_path) { 'new_issue.json' }

    it 'returns the object_attributes.url' do
      expect(subject.url).to eq(event.dig('object_attributes', 'url'))
    end
  end

  describe '#title' do
    it 'returns the resource title' do
      expect(subject.title).to eq(event['object_attributes']['title'])
    end
  end

  describe '#description' do
    it 'returns the resource description' do
      expect(subject.description).to eq(event['object_attributes']['description'])
    end

    context 'when the description is absent' do
      it 'returns an empty string' do
        event['object_attributes']['description'] = ''

        expect(subject.description).to be_empty
      end
    end
  end

  describe '#action' do
    it 'returns the resource action' do
      expect(subject.action).to eq(event['object_attributes']['action'])
    end
  end

  describe '#added_label_names' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns an array of the issue label names' do
        expect(subject.added_label_names).to match_array(labels)
      end
    end

    context 'when the event is an issue update' do
      let(:fixture_path) { 'update_issue.json' }

      it 'returns an array of added label names' do
        expect(subject.added_label_names).to match_array(current_labels - previous_labels)
      end
    end

    context 'with no label changes' do
      it 'returns an empty array' do
        expect(subject.added_label_names).to eq([])
      end
    end
  end

  describe '#removed_label_names' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns an empty array' do
        expect(subject.removed_label_names).to eq([])
      end
    end

    context 'when the event is an issue update' do
      let(:fixture_path) { 'update_issue.json' }

      it 'returns an array of removed label names' do
        expect(subject.removed_label_names).to eq(previous_labels - current_labels)
      end
    end

    context 'whith no label changes' do

      it 'returns an empty array' do
        expect(subject.removed_label_names).to eq([])
      end
    end
  end

  describe '#label_names' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns an array of label names' do
        expect(subject.label_names).to match_array(labels)
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns an array of label names' do
        expect(subject.label_names).to match_array(labels)
      end
    end

    context 'when field labels is absent' do
      let(:fixture_path) { 'new_issue.json' }

      before do
        event.delete('labels')
      end

      it 'returns an empty array' do
        expect(subject.label_names).to eq([])
      end
    end
  end

  describe '#type_label' do
    let(:fixture_path) { 'new_issue.json' }

    context 'when labels include type label' do
      before do
        event['labels'] = [{ 'title' => 'type::foo' }]
      end

      it 'returns the type label' do
        expect(subject.type_label).to eq('type::foo')
      end
    end

    context 'when labels does not include type label' do
      before do
        event['labels'] = [{ 'title' => 'bar::foo' }]
      end

      it 'returns nil' do
        expect(subject.type_label).to be_nil
      end
    end
  end

  describe '#type_label_set?' do
    let(:fixture_path) { 'new_issue.json' }

    context 'when labels include type label' do
      before do
        event['labels'] = [{ 'title' => 'type::foo' }]
      end

      it 'returns true' do
        expect(subject).to be_type_label_set
      end
    end

    context 'when labels does not include type label' do
      before do
        event['labels'] = [{ 'title' => 'bar::foo' }]
      end

      it 'returns false' do
        expect(subject).not_to be_type_label_set
      end
    end
  end

  describe '#from_gitlab_org?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project path begins with gitlab-org/' do
      it 'returns true' do
        event['project']['path_with_namespace'] = 'gitlab-org/gitlab'

        expect(subject.from_gitlab_org?).to eq(true)
      end
    end

    context 'when project path contains but not begins with gitlab-org' do
      it 'returns false' do
        event['project']['path_with_namespace'] = 'gitlab/gitlab-org/gitlab'

        expect(subject.from_gitlab_org?).to eq(false)
      end
    end
  end

  describe '#from_gitlab_com?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project path begins with gitlab-com/' do
      it 'returns true' do
        event['project']['path_with_namespace'] = 'gitlab-com/gitlab'

        expect(subject.from_gitlab_com?).to eq(true)
      end
    end

    context 'when project path contains but not begins with gitlab-com' do
      it 'returns false' do
        event['project']['path_with_namespace'] = 'gitlab/gitlab-com/gitlab'

        expect(subject.from_gitlab_com?).to eq(false)
      end
    end
  end

  describe '#from_gitlab_org_security?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project path begins with gitlab-org/security' do
      it 'returns true' do
        event['project']['path_with_namespace'] = 'gitlab-org/security/gitlab'

        expect(subject.from_gitlab_org_security?).to eq(true)
      end
    end

    context 'when project path contains but not begins with gitlab-org/security' do
      it 'returns false' do
        event['project']['path_with_namespace'] = 'gitlab/gitlab-org/security/gitlab'

        expect(subject.from_gitlab_org_security?).to eq(false)
      end
    end
  end

  describe '#from_gitlab_org_gitlab?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project ID is the gitlab-org/gitlab one' do
      it 'returns true' do
        event['project']['id'] = described_class::GITLAB_PROJECT_ID

        expect(subject.from_gitlab_org_gitlab?).to eq(true)
      end
    end

    context 'when project ID is not the gitlab-org/gitlab one' do
      it 'returns false' do
        event['project']['id'] = described_class::GITLAB_PROJECT_ID.succ

        expect(subject.from_gitlab_org_gitlab?).to eq(false)
      end
    end
  end

  describe '#from_www_gitlab_com?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project ID is the gitlab-com/www-gitlab-com one' do
      it 'returns true' do
        event['project']['id'] = described_class::WWW_GITLAB_COM_PROJECT_ID

        expect(subject.from_www_gitlab_com?).to eq(true)
      end
    end

    context 'when project ID is not the gitlab-com/www-gitlab-com one' do
      it 'returns false' do
        event['project']['id'] = described_class::WWW_GITLAB_COM_PROJECT_ID.succ

        expect(subject.from_www_gitlab_com?).to eq(false)
      end
    end
  end

  describe '#from_runbooks?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project ID is the gitlab-com/runbooks one' do
      it 'returns true' do
        event['project']['id'] = described_class::RUNBOOKS_PROJECT_ID

        expect(subject.from_runbooks?).to eq(true)
      end
    end

    context 'when project ID is not the gitlab-com/runbooks one' do
      it 'returns false' do
        event['project']['id'] = described_class::RUNBOOKS_PROJECT_ID.succ

        expect(subject.from_runbooks?).to eq(false)
      end
    end
  end

  describe '#from_part_of_product_project?' do
    let(:fixture_path) { 'new_issue.json' }
    let(:csv_row) { "#{event['project']['path_with_namespace']},#{event['project']['id']}" }

    before do
      stub_request(:get, Triage::PartOfProductProjects::CSV_URLS[:com])
        .to_return(body: "project_path,project_id\n#{csv_row}")
    end

    context 'when project is in the "part of product" list' do
      it 'returns true' do
        expect(subject.from_part_of_product_project?).to eq(true)
      end
    end

    context 'when project ID is not in the "part of product" list' do
      it 'returns false' do
        event['project']['id'] = event['project']['id'] + 1

        expect(subject.from_part_of_product_project?).to eq(false)
      end
    end
  end

  describe '#automation_author?' do
    let(:fixture_path) { 'new_issue.json' }

    described_class::AUTOMATION_IDS.each do |id|
      context "when author id is '#{id}'" do
        it 'returns true' do
          event['object_attributes']['author_id'] = id

          expect(subject.automation_author?).to eq(true)
        end
      end
    end

    context "when username is 42" do
      it 'returns false' do
        event['object_attributes']['author_id'] = 42

        expect(subject.automation_author?).to eq(false)
      end
    end
  end

  describe '#gitlab_bot_event_actor?' do
    let(:fixture_path) { 'new_issue.json' }

    context 'when actor username is gitlab-bot' do
      it 'returns true' do
        event['user'] = { 'username' => 'gitlab-bot' }

        expect(subject.gitlab_bot_event_actor?).to eq(true)
      end
    end

    context 'when actor username is something else' do
      it 'returns false' do
        event['user'] = { 'username' => 'nice_author' }

        expect(subject.gitlab_bot_event_actor?).to eq(false)
      end
    end
  end

  describe '#author_is_gitlab_service_account?' do
    let(:fixture_path) { 'new_issue.json' }

    before do
      allow(subject).to receive(:resource_author).and_return(Triage::User.new(username: username))
    end

    context "when user username is a project service account" do
      let(:username) { 'project_278964_bot4' }

      it 'returns true' do
        expect(subject.author_is_gitlab_service_account?).to eq(true)
      end
    end

    context "when user username is a group service account" do
      let(:username) { 'group_278964_bot4' }

      it 'returns true' do
        expect(subject.author_is_gitlab_service_account?).to eq(true)
      end
    end

    context "when user username is foo" do
      let(:username) { 'foo' }

      it 'returns false' do
        expect(subject.author_is_gitlab_service_account?).to eq(false)
      end
    end
  end

  describe '#gitlab_org_author?', :clean_cache do
    let(:fixture_path) { 'new_issue.json' }
    let(:author_id) { event['object_attributes']['author_id'] }

    context "when author is a member of the gitlab-org group" do
      it 'returns true' do
        expect_api_requests do |requests|
          requests << stub_api_request(path: "/groups/#{Triage::GITLAB_ORG_GROUP}/members", query: { per_page: 100 }, response_body: [{ id: author_id, username: 'foo' }])

          expect(subject.gitlab_org_author?).to eq(true)
        end
      end
    end

    context "when author is not a member of the gitlab-org group" do
      it 'returns false' do
        expect_api_request(path: "/groups/#{Triage::GITLAB_ORG_GROUP}/members", query: { per_page: 100 }, response_body: []) do
          expect(subject.gitlab_org_author?).to eq(false)
        end
      end
    end
  end

  shared_examples 'wider community author methods', :clean_cache do |group_path, method_name|
    let(:fixture_path) { 'new_issue.json' }
    let(:author_id) { event['object_attributes']['author_id'] }

    before do
      allow(subject).to receive(:automation_author?).and_return(false)
      allow(subject).to receive(:author_is_gitlab_service_account?).and_return(false)
    end

    context "when author is an automation user" do
      before do
        allow(subject).to receive(:automation_author?).and_return(true)
      end

      it 'returns false' do
        expect(subject.public_send(method_name)).to eq(false)
      end
    end

    context "when user is a gitlab service account" do
      before do
        allow(subject).to receive(:author_is_gitlab_service_account?).and_return(true)
      end

      it 'returns false' do
        expect(subject.public_send(method_name)).to eq(false)
      end
    end

    context "when author is a member of the #{group_path} group" do
      it 'returns false' do
        expect_api_requests do |requests|
          requests << stub_api_request(path: "/groups/#{group_path}/members", query: { per_page: 100 }, response_body: [{ id: author_id, username: 'foo' }])
          requests << stub_api_request(path: "/groups/#{CGI.escape(Triage::GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP)}/members", query: { per_page: 100 }, response_body: [])

          expect(subject.public_send(method_name)).to eq(false)
        end
      end

      context "and a community core team member" do
        it 'returns true' do
          expect_api_requests do |requests|
            requests << stub_api_request(path: "/groups/#{group_path}/members", query: { per_page: 100 }, response_body: [{ id: author_id, username: 'foo' }])
            requests << stub_api_request(path: "/groups/#{CGI.escape(Triage::GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP)}/members", query: { per_page: 100 }, response_body: [{ id: author_id, username: 'foo' }])

            expect(subject.public_send(method_name)).to eq(true)
          end
        end
      end
    end

    context "when author is not a member of the #{group_path} group" do
      it 'returns true' do
        expect_api_request(path: "/groups/#{group_path}/members", query: { per_page: 100 }, response_body: []) do
          expect(subject.public_send(method_name)).to eq(true)
        end
      end
    end
  end

  describe '#wider_community_author?' do
    it_behaves_like 'wider community author methods', Triage::GITLAB_ORG_GROUP, 'wider_community_author?'
  end

  describe '#wider_gitlab_com_community_author?' do
    it_behaves_like 'wider community author methods', Triage::GITLAB_COM_GROUP, 'wider_gitlab_com_community_author?'
  end

  describe '#jihu_contributor?', :clean_cache do
    let(:fixture_path) { 'new_merge_request.json' }

    context 'when author is a JiHu team member' do
      before do
        allow(Triage).to receive(:jihu_team_member_ids)
          .and_return([event.dig('object_attributes', 'author_id')])
      end

      it 'returns true' do
        expect(subject.jihu_contributor?).to eq(true)
      end
    end

    context 'when author is not a JiHu team member' do
      before do
        allow(Triage).to receive(:jihu_team_member_ids).and_return([])
      end

      it 'returns false' do
        expect(subject.jihu_contributor?).to eq(false)
      end
    end
  end

  describe '#project_id' do
    it 'returns the project id' do
      expect(subject.project_id).to eq(event['project']['id'])
    end
  end

  describe '#project_web_url' do
    it 'returns the project id' do
      expect(subject.project_web_url).to eq(event['project']['web_url'])
    end
  end

  describe '#with_project_id?' do
    context 'when project id matches' do
      it 'returns true' do
        expect(subject.with_project_id?(event['project']['id'])).to eq(true)
      end
    end

    context 'when project id does not match' do
      it 'returns false' do
        expect(subject.with_project_id?(42)).to eq(false)
      end
    end
  end

  describe '#project_path_with_namespace' do
    it 'returns the project path with namespace' do
      expect(subject.project_path_with_namespace).to eq(event['project']['path_with_namespace'])
    end
  end

  shared_examples 'a Triage::IssuableEvent' do
    describe '#iid' do
      it 'returns the issue iid' do
        expect(subject.iid).to eq(event.dig('object_attributes', 'iid'))
      end
    end

    describe '#resource_open?' do
      context 'when the resource is opened' do
        it 'returns true' do
          expect(subject.resource_open?).to be_truthy
        end
      end

      %w[closed merged].each do |mr_state|
        context "when the resource is #{mr_state}" do
          it 'returns false' do
            event['object_attributes']['state'] = mr_state

            expect(subject.resource_open?).to be_falsey
          end
        end
      end
    end
  end

  describe Triage::IssueEvent do
    let(:fixture_path) { 'new_issue.json' }

    it_behaves_like 'a Triage::IssuableEvent'

    describe '#noteable_path' do
      it 'returns the API URL for the issue' do
        expect(subject.noteable_path)
          .to match(%r{\A/projects/\d+/issues/\d+\z})
      end
    end

    describe '#weight' do
      it 'returns the weight in the issue' do
        expect(subject.weight).to eq(event['object_attributes']['weight'])
      end
    end
  end

  describe Triage::MergeRequestEvent do
    let(:fixture_path) { 'new_merge_request.json' }

    it_behaves_like 'a Triage::IssuableEvent'

    describe '#wip?' do
      it 'returns work_in_progress from the event' do
        expect(subject.wip?).to eq(event['object_attributes']['work_in_progress'])
      end
    end

    describe '#noteable_path' do
      it 'returns the API URL for the merge request' do
        expect(subject.noteable_path)
          .to match(%r{\A/projects/\d+/merge_requests/\d+\z})
      end
    end

    describe '#merge_event?' do
      it 'returns true if it action is merge' do
        event['object_attributes']['action'] = 'merge'

        expect(subject.merge_event?).to eq(true)
      end

      it 'returns false if action is not merge' do
        event['object_attributes']['action'] = 'open'

        expect(subject.merge_event?).to eq(false)
      end
    end

    describe '#revision_update?' do
      it 'returns true if action is update and it has oldrev' do
        event['object_attributes']['action'] = 'update'
        event['object_attributes']['oldrev'] = 'somerevision'

        expect(subject.revision_update?).to eq(true)
      end

      it 'returns false if action is open' do
        event['object_attributes']['action'] = 'open'

        expect(subject.revision_update?).to eq(false)
      end
    end

    describe '#approval_event?' do
      it 'returns true if the action is approval' do
        event['object_attributes']['action'] = 'approval'

        expect(subject.approval_event?).to eq(true)
      end

      it 'returns false if action is not approval' do
        event['object_attributes']['action'] = 'approved'

        expect(subject.approval_event?).to eq(false)
      end
    end

    describe '#approved_event?' do
      it 'returns true if the action is approved' do
        event['object_attributes']['action'] = 'approved'

        expect(subject.approved_event?).to eq(true)
      end

      it 'returns false if action is not approved' do
        event['object_attributes']['action'] = 'approval'

        expect(subject.approved_event?).to eq(false)
      end
    end

    describe '#source_branch_is?' do
      it 'returns true if the source_branch matches' do
        event['object_attributes']['source_branch'] = '14-9-stable-ee'

        expect(subject.source_branch_is?('14-9-stable-ee')).to eq(true)
      end

      it 'returns false if the source_branch does not match' do
        event['object_attributes']['source_branch'] = 'master'

        expect(subject.source_branch_is?('14-9-stable-ee')).to eq(false)
      end
    end

    describe '#target_branch_is_stable_branch?' do
      it 'returns true if the target_branch has the stable-ee suffix' do
        event['object_attributes']['target_branch'] = '14-9-stable-ee'

        expect(subject.target_branch_is_stable_branch?).to eq(true)
      end

      it 'returns false if the target_branch does not have the stable-ee suffix' do
        event['object_attributes']['target_branch'] = 'master'

        expect(subject.target_branch_is_stable_branch?).to eq(false)
      end
    end

    describe '#last_commit_sha' do
      it 'returns SHA if last commit ID is present' do
        expect(subject.last_commit_sha).to eq('58b824444e5b8fbcc0376d7dd37ed6684291f278')
      end

      it 'returns nil if last commit ID is not present' do
        event['object_attributes']['last_commit'] = nil

        expect(subject.last_commit_sha).to be_nil
      end
    end
  end

  describe Triage::NoteEvent do
    shared_examples 'a Triage::NoteEvent' do |resource_type|
      describe '#iid' do
        it 'returns the issue iid' do
          expect(subject.iid).to eq(event.dig(resource_type, 'iid'))
        end
      end

      describe '#resource_open?' do
        context 'when the noteable is opened' do
          it 'returns true' do
            expect(subject.resource_open?).to be_truthy
          end
        end

        %w[closed merged].each do |mr_state|
          context "when the noteable is #{mr_state}" do
            it 'returns false' do
              event[resource_type]['state'] = mr_state

              expect(subject.resource_open?).to be_falsey
            end
          end
        end
      end

      describe '#key' do
        context "when the event is an #{resource_type} note" do
          it "returns #{resource_type}.note" do
            expect(subject.key).to eq("#{resource_type}.note")
          end
        end
      end

      describe '#created_at' do
        it 'returns the note created_at time' do
          expect(subject.created_at).to eq(Time.parse(event['object_attributes']['created_at']))
        end
      end

      describe '#new_comment' do
        it 'returns the new note content' do
          expect(subject.new_comment).to eq(event['object_attributes']['description'])
        end
      end

      describe '#noteable_author_id' do
        it 'returns the noteable author id' do
          expect(subject.noteable_author_id).to eq(event[resource_type]['author_id'])
        end
      end

      describe '#noteable_author' do
        let(:user_attrs) { { 'id' => event[resource_type]['author_id'], 'username' => 'root' } }

        it 'returns a User object that corresponds to the object_attributes.author_id' do
          expect_api_request(path: "/users/#{user_attrs['id']}", response_body: user_attrs) do
            expect(subject.noteable_author).to be_a(Triage::User)
            expect(subject.noteable_author.to_h).to eq(user_attrs)
            expect(subject.noteable_author.id).to eq(user_attrs['id'])
            expect(subject.noteable_author.username).to eq(user_attrs['username'])
          end
        end
      end

      describe '#by_noteable_author?' do
        let(:noteable_author_id) { 1 }

        context 'note is created by the noteable author' do
          it 'returns true' do
            event[resource_type]['author_id'] = event['user']['id']

            expect(subject.by_noteable_author?).to eq(true)
          end
        end

        context 'note is not created by the noteable author' do
          it 'returns true' do
            event[resource_type]['author_id'] = event['user']['id'] + 1

            expect(subject.by_noteable_author?).to eq(false)
          end
        end
      end
    end

    context 'when the note is on an issue' do
      let(:fixture_path) { 'note_on_issue.json' }

      it_behaves_like 'a Triage::NoteEvent', 'issue'

      describe '#label_names' do
        let(:labels) { event.dig('issue', 'labels').map { |label| label['title'] } }

        it 'returns an array of label names' do
          expect(subject.label_names).to match_array(labels)
        end
      end

      describe '#noteable_path' do
        it 'returns the API URL for the issue' do
          expect(subject.noteable_path)
            .to match(%r{\A/projects/\d+/issues/\d+\z})
        end
      end

      describe '#note_on_issue?' do
        it 'returns true' do
          expect(subject.note_on_issue?).to eq(true)
        end
      end

      describe '#note_on_merge_request?' do
        it 'returns true' do
          expect(subject.note_on_merge_request?).to eq(false)
        end
      end
    end

    context 'when the note is on a merge request' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      it_behaves_like 'a Triage::NoteEvent', 'merge_request'

      describe '#label_names' do
        let(:labels) { event.dig('merge_request', 'labels').map { |label| label['title'] } }

        it 'returns an array of label names' do
          expect(subject.label_names).to match_array(labels)
        end
      end

      describe '#noteable_path' do
        it 'returns the API URL for the merge request' do
          expect(subject.noteable_path)
            .to match(%r{\A/projects/\d+/merge_requests/\d+\z})
        end
      end

      describe '#note_on_issue?' do
        it 'returns true' do
          expect(subject.note_on_issue?).to eq(false)
        end
      end

      describe '#note_on_merge_request?' do
        it 'returns true' do
          expect(subject.note_on_merge_request?).to eq(true)
        end
      end
    end

    context 'when the note is on an unknown noteable_type' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      before do
        event['object_attributes']['noteable_type'] = 'foo'
      end

      describe '#label_names' do
        it 'returns an array of label names' do
          expect { subject.label_names }.to raise_error(described_class::UnknownNoteableTypeError, "`foo` is an unknown noteable_type!")
        end
      end
    end
  end
end
