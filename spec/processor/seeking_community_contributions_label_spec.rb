# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/seeking_community_contributions_label'

RSpec.describe Triage::SeekingCommunityContributionsLabel do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'update',
        added_label_names: ['Seeking community contributions'],
        description: 'A fake issue description with no implementation guide.',
        from_gitlab_org?: true
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.close", "issue.note", "issue.open", "issue.reopen", "issue.update"]

  describe '#applicable?' do
    context 'when event is from gitlab org, for the Seeking community contributions label being added, on an issue without an implementation guide' do
      include_examples 'event is applicable'
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not for the seeking community contributions label being added' do
      before do
        allow(subject).to receive(:seeking_community_contribution_label_added?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event description is not missing an implementation guide' do
      before do
        allow(subject).to receive(:issue_missing_implementation_guide?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'posts implementation guide comment' do
      body = <<~MARKDOWN.chomp
        @root thanks for adding the ~"Seeking community contributions" label!
          
        This issue's description does not seem to have a section for "Implementation Guide". 
        Please consider adding one, because it makes a [big difference for contributors](https://about.gitlab.com/handbook/engineering/development/dev/create-editor/community-contributions/#treat-wider-community-as-primary-audience).
        This section can be brief but must have clear technical guidance, like:
        
        - Hints on lines of code which may need changing
        - Hints on similar code/patterns that can be leveraged
        - Suggestions for test coverage
        - Ideas for breaking up the merge requests into iterative chunks
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
