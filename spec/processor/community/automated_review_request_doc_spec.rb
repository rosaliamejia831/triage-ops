# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/automated_review_request_doc'

RSpec.describe Triage::AutomatedReviewRequestDoc do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        from_gitlab_org?: true,
        wider_community_author?: true,
        project_id: project_id,
        iid: merge_request_iid,
        wip?: false,
        project_web_url: 'https://gitlab.example/group/project'
      }
    end
    let(:project_id) { 123 }
    let(:merge_request_iid) { 300 }
  end

  let(:approvers) { %w[tech_writer1 tech_writer2] }
  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "app/models/misplaced/doc/user.md",
          "new_path" => "doc/user.md"
        }
      ]
    }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
    stub_api_request(
      path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: [])
    allow_any_instance_of(Triage::DocumentationCodeOwner).to receive(:approvers).and_return(approvers)
  end

  include_examples 'registers listeners', ["merge_request.update", "merge_request.open"]

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when MR is not opened' do
      before do
        allow(event).to receive(:resource_open?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when MR is a WIP' do
      before do
        allow(event).to receive(:wip?).and_return(true)
      end

      include_examples 'event is not applicable'
    end

    context 'when MR has Technical Writing label' do
      let(:label_names) { ['Technical Writing'] }

      include_examples 'event is not applicable'
    end

    context 'when MR has tw::triaged label' do
      let(:label_names) { ['tw::triaged'] }

      include_examples 'event is not applicable'
    end

    context 'when MR has tw::doing label' do
      let(:label_names) { ['tw::doing'] }

      include_examples 'event is not applicable'
    end

    context 'when event is not from a wider community author' do
      before do
        allow(event).to receive(:wider_community_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request does not change docs' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              'old_path' => 'app/models/user.rb',
              'new_path' => 'app/models/user_renamed.rb'
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    shared_examples 'process docs merge request' do
      let(:extra_message) { "" }

      it 'posts a comment mentioning technical writer and labeling the merge request' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          Hi #{approvers.map { |username| "@#{username}" }.join(' ')}! Please review this ~"documentation" merge request.
          #{extra_message}
          /label ~"documentation" ~"tw::triaged"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when event is applicable' do
      include_examples 'process docs merge request'

      context 'when approvers include @gl-docsteam' do
        include_examples 'process docs merge request' do
          let(:approvers) { [described_class::GL_DOCSTEAM_HANDLE] }
          let(:extra_message) { "\nPlease also consider updating the `CODEOWNERS` file in the #{event.project_web_url} project.\n" }
        end
      end
    end

    context 'when there are no matching approvers' do
      let(:approvers) { [] }

      it 'only labels the merge request' do
        body = <<~MARKDOWN.chomp
          /label ~"documentation" ~"tw::triaged"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when merge request changes docs/' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              'old_path' => 'docs/index.md',
              'new_path' => 'docs/index.md'
            }
          ]
        }
      end

      include_examples 'process docs merge request'
    end
  end
end
