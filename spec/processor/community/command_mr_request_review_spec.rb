# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_request_review'

RSpec.describe Triage::CommandMrRequestReview do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        object_kind: 'note',
        noteable_type: 'merge_request',
        from_gitlab_org?: true,
        wider_community_author?: true,
        by_noteable_author?: true,
        with_project_id?: false,
        new_comment: %(@gitlab-bot request_review),
        payload: {
          'merge_request' => {
            'work_in_progress' => draft_mr
          }
        }
      }
    end
  end

  let(:roulette) do
    [
      { 'username' => 'coach', 'merge_request_coach' => true, 'available' => true },
      { 'username' => 'unavailable_coach', 'merge_request_coach' => true, 'available' => false },
      { 'username' => 'not_coach', 'merge_request_coach' => false, 'available' => true }
    ]
  end

  let(:draft_mr) { false }

  before do
    allow(WwwGitLabCom).to receive(:roulette).and_return(roulette)
  end

  subject { described_class.new(event) }

  it_behaves_like 'registers listeners', ['merge_request.note']
  it_behaves_like 'command processor', 'request_review'

  describe '#applicable?' do
    context 'when event is from gitlab org, for a note on a new merge request, by the mr author' do
      include_examples 'event is applicable'
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event author is not the noteable author' do
      before do
        allow(event).to receive(:by_noteable_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not from a wider community author' do
      before do
        allow(event).to receive(:wider_community_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    let(:body) do
      <<~MARKDOWN.chomp
        Hey there @coach, can you please review this MR or find another reviewer?
        /assign_reviewer @coach
      MARKDOWN
    end

    it 'posts a comment to reply to author and pings MR coaches' do
      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end

    context 'when the command was issued inside the gitlab chart project' do
      before do
        allow(event).to receive(:with_project_id?).with(described_class::GITLAB_CHART_PROJECT_ID).and_return(true)
      end

      context 'when the parent MR is a draft' do
        let(:draft_mr) { true }

        it 'does not set ~"workflow::ready for review"' do
          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end

      context 'when the parent MR is not a draft' do
        let(:body) do
          <<~MARKDOWN.chomp
            Hey there @coach, can you please review this MR or find another reviewer?
            /assign_reviewer @coach
            /label ~"workflow::ready for review"
          MARKDOWN
        end

        it 'sets ~"workflow::ready for review"' do
          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end
    end

    it_behaves_like 'rate limited', count: 1, period: 86_400
  end
end
