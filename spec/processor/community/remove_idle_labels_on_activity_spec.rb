# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/remove_idle_labels_on_activity'

RSpec.describe Triage::RemoveIdleLabelOnActivity do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        object_kind: 'note',
        noteable_type: 'merge_request',
        from_gitlab_org?: from_gitlab_org,
        gitlab_bot_event_actor?: bot_author,
      }
    end
  end

  let(:label_names) { [described_class::COMMUNITY_CONTRIBUTION_LABEL, described_class::IDLE_LABEL] }
  let(:from_gitlab_org) { true }
  let(:bot_author) { false }

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.note']

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when the comment is from the bot' do
      let(:bot_author) { true }

      include_examples 'event is not applicable'
    end

    context 'when there is no `community contribution` label' do
      let(:label_names) { [described_class::IDLE_LABEL] }

      include_examples 'event is not applicable'
    end

    context 'when there is no idle or stale label' do
      let(:label_names) { [described_class::COMMUNITY_CONTRIBUTION_LABEL] }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'posts a message to remove the labels' do
      expected_message =<<~MARKDOWN.chomp
        /unlabel ~"#{described_class::IDLE_LABEL}" ~"#{described_class::STALE_LABEL}"
      MARKDOWN

      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end
    end
  end
end
