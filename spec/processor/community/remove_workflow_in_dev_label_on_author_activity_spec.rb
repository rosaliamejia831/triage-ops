# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/remove_workflow_in_dev_label_on_author_activity'

RSpec.describe Triage::RemoveWorkflowInDevLabelOnAuthorActivity do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        object_kind: 'note',
        noteable_type: 'merge_request',
        from_gitlab_org?: true,
        by_noteable_author?: true
      }
    end
    let(:label_names) { [described_class::WORKFLOW_IN_DEV_LABEL] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.note']


  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when the event is not by author' do
      before do
        allow(event).to receive(:by_noteable_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when there is no "workflow::in dev" label' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end

    context 'when there is the "workflow::in dev" label' do
      include_examples 'event is applicable'
    end

    context 'when there is the legacy pingpong label' do
      let(:label_names) { [described_class::LEGACY_PINGPONG_LABEL] }

      include_examples 'event is applicable'
    end
  end

  describe '#process' do
    it 'posts a message to remove the label' do
      expected_message =<<~MARKDOWN.chomp
        /unlabel ~"#{described_class::WORKFLOW_IN_DEV_LABEL}" ~"#{described_class::LEGACY_PINGPONG_LABEL}"
      MARKDOWN

      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end
    end
  end
end
