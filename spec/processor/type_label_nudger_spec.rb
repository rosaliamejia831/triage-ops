# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/type_label_nudger'
require_relative '../../triage/triage/event'

RSpec.describe Triage::TypeLabelNudger do
  include_context 'with event', 'Triage::IssuableEvent' do
    let(:event_attrs) do
      {
        action: 'open',
        object_kind: 'merge_request',
        with_project_id?: true
      }
    end
  end

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  let(:discussion_id) { 999 }

  let(:merge_request_discussions) do
    [id: discussion_id, notes: merge_request_notes]
  end

  before do
    stub_api_request(
      path: '/projects/123/merge_requests/456/discussions',
      query: { per_page: 100 },
      response_body: merge_request_discussions
    )
  end

  describe '#applicable?' do
    context 'when it is a community contribution' do
      let(:label_names) { ['Community contribution', 'another_label'] }

      include_examples 'event is not applicable'
    end

    context 'when the command was not issued inside an allowed project' do
      before do
        allow(event).to receive(:with_project_id?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when there are no `work type` labels' do
      let(:label_names) { ['another_label'] }

      context 'when there is no discussion from the processor' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: 'review discussion 2' }
          ]
        end

        include_examples 'event is applicable'
      end

      context 'when there is a discussion from the processor' do
        context 'when there is a resolved discussion from the processor' do
          let(:merge_request_notes) do
            [
              { body: 'review discussion 1' },
              { body: "#{comment_mark} processor message", resolved: true }
            ]
          end

          include_examples 'event is applicable'
        end

        context 'when there is an unresolved discussion from the processor' do
          let(:merge_request_notes) do
            [
              { body: 'review discussion 1' },
              { body: "#{comment_mark} processor message", resolved: false }
            ]
          end

          include_examples 'event is not applicable'
        end
      end
    end

    context 'when there are `work type` labels' do
      let(:label_names) { ['bug::mobile'] }

      context 'when there is no previous discussion from the processor' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: 'review discussion 2' }
          ]
        end

        include_examples 'event is not applicable'
      end

      context 'when there is a discussion from the processor' do
        context 'when there is a resolved discussion from the processor' do
          let(:merge_request_notes) do
            [
              { body: 'review discussion 1' },
              { body: "#{comment_mark} processor message", resolved: true }
            ]
          end

          include_examples 'event is not applicable'
        end

        context 'when there is an unresolved discussion from the processor' do
          let(:merge_request_notes) do
            [
              { body: 'review discussion 1' },
              { body: "#{comment_mark} processor message", resolved: false }
            ]
          end

          include_examples 'event is applicable'
        end
      end
    end

    # Purpose: test that the list of allowed projects is respected
    describe 'allowed projects' do
      # Make all other conditions successful, to only test the allowed projects
      let(:label_names) { ['another_label'] }
      let(:merge_request_notes) do
        [
          { body: 'review discussion 1' },
          { body: 'review discussion 2' }
        ]
      end

      described_class::ALLOWED_PROJECTS_IDS.each do |allowed_project_id|
        context "when the command was issued from project ##{allowed_project_id}" do
          before do
            allow(event).to receive(:with_project_id?).and_return(false)
            allow(event).to receive(:with_project_id?).with(allowed_project_id).and_return(true)
          end

          include_examples 'event is applicable'
        end
      end

      context 'when the command was issued from project #999' do
        before do
          allow(event).to receive(:with_project_id?).and_return(false)
          allow(event).to receive(:with_project_id?).with(999).and_return(true)
        end

        include_examples 'event is not applicable'
      end
    end
  end

  describe '#process' do
    context 'when there are no `work type` labels' do
      let(:label_names) { ['unrelated-label'] }

      context 'when no discussions have been created by the processor' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: 'review discussion 2' }
          ]
        end

        it 'schedules a TypeLabelNudgerJob 5 minutes later' do
          expect(Triage::TypeLabelNudgerJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)

          subject.process
        end
      end

      context 'when a previous discussion has been created by the processor' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: "#{comment_mark} processor message", resolved: resolved }
          ]
        end

        context 'when that discussion is not resolved' do
          let(:resolved) { false }

          it 'does not schedule a TypeLabelNudgerJob' do
            expect(Triage::TypeLabelNudgerJob).not_to receive(:perform_in)

            subject.process
          end

          it 'does nothing' do
            subject.process
          end
        end

        context 'when that discussion is resolved' do
          let(:resolved) { true }

          it 'unresolves the previous discussion' do
            expect_api_request(verb: :put, path: "/projects/123/merge_requests/456/discussions/#{discussion_id}", request_body: { 'resolved' => false }) do
              subject.process
            end
          end
        end
      end
    end

    context 'when there are `work type` labels' do
      let(:label_names) { ['maintenance::refactor'] }

      context 'when no discussions have been created by the processor' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: 'review discussion 2' }
          ]
        end

        it 'does nothing' do
          subject.process
        end
      end

      context 'when a previous discussion has been created by the processor' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: "#{comment_mark} processor message", resolved: resolved }
          ]
        end

        context 'when that discussion is not resolved' do
          let(:resolved) { false }

          it 'resolves the previous discussion' do
            expect_api_request(verb: :put, path: "/projects/123/merge_requests/456/discussions/#{discussion_id}", request_body: { 'resolved' => true }) do
              subject.process
            end
          end
        end

        context 'when that discussion is resolved' do
          let(:resolved) { true }

          it 'does nothing' do
            subject.process
          end
        end
      end
    end
  end
end
