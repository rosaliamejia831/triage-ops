# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/appsec/approved_by_appsec'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::ApprovedByAppSec, :clean_cache do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:approver_username) { 'approver' }
    let(:approver_user_id) { 4242 }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org?: true,
        jihu_contributor?: true,
        gitlab_bot_event_actor?: false,
        event_actor_username: approver_username,
        event_actor_id: approver_user_id,
        last_commit_sha: 'deadbeaf',
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/groups/#{CGI.escape(Triage::GITLAB_COM_APPSEC_GROUP)}/members",
      query: { per_page: 100 },
      response_body: [username: approver_username, id: approver_user_id])
  end

  include_examples 'registers listeners',
    ['merge_request.approval', 'merge_request.approved']

  describe '#applicable' do
    context 'when event is not from gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request is not a JiHu contribution' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when it is not approved by AppSec' do
      before do
        allow(event).to receive(:event_actor_id).and_return(approver_user_id.succ)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    let(:discussion_id) { 'approval_discussion_id' }
    let(:discussions_path) { "/projects/#{project_id}/merge_requests/#{merge_request_iid}/discussions" }
    let(:approval_discussion_path) { "#{discussions_path}/#{discussion_id}" }

    let(:merge_request_notes) do
      [
        { body: 'review comment 1' },
        { body: comment_mark }
      ]
    end

    let(:merge_request_discussions) do
      [ id: discussion_id, notes: merge_request_notes ]
    end

    context 'when there is already a AppSec thread' do
      before do
        stub_api_request(
          path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/discussions",
          query: { per_page: 100 },
          response_body: merge_request_discussions)
      end

      it 'appends a new note to the discussion and resolves it' do
        expect_api_requests do |requests|
          # Appends the new note to the approval discussion
          requests << stub_api_request(
            verb: :post,
            path: "#{approval_discussion_path}/notes",
            request_body: { body: <<~MARKDOWN.strip })
            #{comment_mark}
            :white_check_mark: AppSec approved for deadbeaf
            by `@#{event.event_actor_username}`, adding ~"sec-planning::complete"

            /label ~"sec-planning::complete"
          MARKDOWN

          # Resolve the approval discussion
          requests << stub_api_request(verb: :put, path: approval_discussion_path, request_body: { 'resolved' => true })

          subject.process
        end
      end
    end

    context 'when there is no AppSec thread' do
      before do
        stub_api_request(
          path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/discussions",
          query: { per_page: 100 },
          response_body: [])
          .times(1).then
          .to_return(body: JSON.dump(merge_request_discussions))
      end

      it 'creates a new discussion and resolves it' do
        expect_api_requests do |requests|
          # Creates a new discussion
          requests << stub_api_request(
            verb: :post,
            path: discussions_path,
            request_body: { body: <<~MARKDOWN.strip })
            #{comment_mark}
            :white_check_mark: AppSec approved for deadbeaf
            by `@#{event.event_actor_username}`, adding ~"sec-planning::complete"

            /label ~"sec-planning::complete"
          MARKDOWN

          # Resolve the approval discussion
          requests << stub_api_request(verb: :put, path: approval_discussion_path, request_body: { 'resolved' => true })

          subject.process
        end
      end
    end
  end
end
