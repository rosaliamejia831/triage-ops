# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/appsec/revoke_appsec_approval'

RSpec.describe Triage::RevokeAppSecApproval do
  let(:from_gitlab_org) { nil }
  let(:jihu_contributor) { nil }
  let(:revision_update) { nil }
  let(:label_names) { nil }

  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'update',
        from_gitlab_org?: from_gitlab_org,
        jihu_contributor?: jihu_contributor,
        gitlab_bot_event_actor?: false,
        revision_update?: revision_update,
        label_names: label_names
      }
    end
  end

  subject { described_class.new(event) }

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when event project is under gitlab-org' do
      let(:from_gitlab_org) { true }

      context 'but is not a JiHu contribution' do
        let(:jihu_contributor) { false }

        include_examples 'event is not applicable'
      end

      context 'and is a JiHu contribution' do
        let(:jihu_contributor) { true }

        context 'but is not updating revision' do
          let(:revision_update) { false }

          include_examples 'event is not applicable'
        end

        context 'and is updating revision' do
          let(:revision_update) { true }

          context 'but does not have AppSec approval' do
            include_examples 'event is not applicable'
          end

          context 'and has AppSec approval' do
            let(:label_names) { [described_class::APPSEC_APPROVAL_LABEL] }

            include_examples 'event is applicable'
          end
        end
      end
    end
  end

  describe '#process' do
    let(:base_path) { '/projects/123/merge_requests/456' }
    let(:user_hash) { { 'id' => 987, 'username' => username } }
    let(:approvals) { { 'approved_by' => [{ 'user' => user_hash }] } }
    let(:appsec_members) { [user_hash] }
    let(:username) { 'joe' }
    let(:username_formatted) { "@#{username}" }
    let(:appsec_group_name_escaped) { CGI.escape(Triage::GITLAB_COM_APPSEC_GROUP) }
    let(:message) do
      <<~MESSAGE.strip
        <!-- triage-serverless PingAppSecOnApproval -->
        :x: Some new changes have been pushed since AppSec last approved.

        Can you please review again #{username_formatted}?

        To approve this again, please un-approve and approve again.
        ~"sec-planning::complete" will be added automatically after
        being re-approved. Here's the quick actions to do this:

        ```
        /unapprove
        /approve
        ```

        /label ~"sec-planning::pending-followup"
        /unlabel ~"sec-planning::complete"
      MESSAGE
    end

    before do
      stub_api_request(path: "/groups/#{appsec_group_name_escaped}/members", response_body: appsec_members, query: { per_page: 100 })
    end

    context "where there is isn't a previous AppSec discussion" do
      it 'creates a new discussion' do
        expect_api_requests do |requests|
          requests << stub_api_request(path: "#{base_path}/discussions", response_body: [], query: { per_page: 100 })
          requests << stub_api_request(path: "#{base_path}/approvals", response_body: approvals)
          requests << stub_api_request(verb: :post, path: "#{base_path}/discussions", request_body: { 'body' => message })

          subject.process
        end
      end
    end

    context 'where there is a previous AppSec discussion' do
      let(:discussion_id) { 729 }

      shared_examples 'unresolves the existing discussion and adds a new comment' do |and_pings|
        it and_pings do
          expect_api_requests do |requests|
            requests << stub_api_request(path: "#{base_path}/discussions", response_body: [{ 'id' => discussion_id, 'notes' => [ { 'body' => message }] }], query: { per_page: 100 })
            requests << stub_api_request(verb: :put, path: "#{base_path}/discussions/#{discussion_id}", request_body: { 'resolved' => false })
            requests << stub_api_request(path: "#{base_path}/approvals", response_body: approvals)
            requests << stub_api_request(verb: :post, path: "#{base_path}/discussions/#{discussion_id}/notes", request_body: { 'body' => message })

            subject.process
          end
        end
      end

      it_behaves_like 'unresolves the existing discussion and adds a new comment', 'and pings the previous approver'

      context 'when we cannot find who previously approved' do
        let(:approvals) { { 'approved_by' => [] } }
        let(:username_formatted) { "@#{Triage::GITLAB_COM_APPSEC_GROUP}" }

        it_behaves_like 'unresolves the existing discussion and adds a new comment', 'and pings the AppSec group'
      end
    end
  end
end
