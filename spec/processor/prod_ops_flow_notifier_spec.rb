# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/prod_ops_flow_notifier'
require_relative '../../triage/triage/event'

RSpec.describe Triage::ProdOpsFlowNotifier do
  include_context 'with event', 'Triage::IssuableEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        project_id: project_id,
        iid: merge_request_iid,
        from_gitlab_org?: true
      }
    end
  end

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "something/old.md",
          "new_path" => "something/new.md"
        }
      ]
    }
  end

  include_context 'with merge request notes'

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.update', 'merge_request.open']

  describe '#applicable?' do
    context 'when relevant files are changed but event project is not gitlab-com/www-gitlab-com' do
      let(:project_id) { 12345 }
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "sites/handbook/source/handbook/marketing/blog/release-posts/",
              "new_path" => ".gitlab/issue_templates/product-development-retro.md"
            }
          ]
        }
      end

      before do
        allow(event).to receive(:from_www_gitlab_com?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event project is gitlab-com/www-gitlab-com' do
      let(:project_id) { Triage::Event::WWW_GITLAB_COM_PROJECT_ID }

      before do
        allow(event).to receive(:from_www_gitlab_com?).and_return(true)
      end

      context 'and there was no change to relevant files' do
        include_examples 'event is not applicable'
      end

      context 'and there is already a comment for the same purpose' do
        let(:merge_request_notes) do
          [
            { body: 'review comment 1' },
            { body: comment_mark }
          ]
        end

        include_examples 'event is not applicable'
      end

      context 'and there were changes to relevant files' do
        let(:merge_request_changes) do
          {
            'changes' => [
              {
                "old_path" => "sites/handbook/source/handbook/marketing/blog/release-posts/foo.md",
                "new_path" => "something/new.md"
              }
            ]
          }
        end

        include_examples 'event is applicable'
      end
    end
  end

  describe '#process' do
    it 'posts a review request comment' do
      body = <<~MARKDOWN.chomp
        #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
        @brhea @fseifoddini please review this Product Operations related Merge Request.
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end

    context 'when changed files are within LABELS_FOR_FILE' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => ".gitlab/merge_request_templates/Release-Post.md",
              "new_path" => ".gitlab/merge_request_templates/Release-Post.md"
            }
          ]
        }
      end

      it 'adds labels defined in LABELS_FOR_FILE' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          @brhea @fseifoddini please review this Product Operations related Merge Request.

          /label ~"Product Operations" ~"prodops:release" ~"release post" ~"templates"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
