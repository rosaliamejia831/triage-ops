# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/rack/dashboard'

describe Triage::Rack::Dashboard do
  subject { described_class.new }

  let(:env) { {} }

  describe '#call' do
    it 'responds with 200 and stats in JSON' do
      status, _headers, body = subject.call(env)

      expect(JSON.parse(body.to_a.join)).to be_kind_of(Hash)
      expect(status).to eq(200)
    end

    context 'when cache is fresh' do
      it 'does not query the stats again' do
        expect(SuckerPunch::Queue).to receive(:stats).once

        2.times { subject.call(env) }
      end
    end

    context 'when cache is stale' do
      it 'queries the stats again' do
        expect(SuckerPunch::Queue).to receive(:stats).twice

        subject.call(env)

        Timecop.travel(Time.now + described_class::STATS_EXPIRES_IN + 1) do
          subject.call(env)
        end
      end
    end
  end
end
