RSpec.shared_examples 'customer contribution processor #applicable?' do
  include_examples 'event is applicable'

  context 'when not on gitlab-org group' do
    before do
      allow(event).to receive(:from_gitlab_org?).and_return(false)
    end

    include_examples 'event is not applicable'
  end

  context 'when not a wider community contribution' do
    before do
      allow(event).to receive(:wider_community_author?).and_return(false)
    end

    include_examples 'event is not applicable'
  end

  context 'when not from a customer' do
    before do
      allow(subject).to receive(:org_name).and_return(nil)
    end

    include_examples 'event is not applicable'
  end
end

RSpec.shared_examples 'customer contribution processor #process' do
  context 'when event is for a MR authored by a customer author with org under the gitlab-org group' do
    it_behaves_like 'slack message posting'
  end

  context 'type labels for MR' do
    context 'type::feature and type::bug labels preferred when multiple' do
      %w[type::feature type::bug].each do |label|
        let(:contribution_type) { label }
        let(:type_label) { ['documentation', contribution_type] }

        it_behaves_like 'slack message posting'
      end
    end

    context 'documentation still mentioned if only type label' do
      let(:type_label) { 'documentation' }

      it_behaves_like 'slack message posting'
    end
  end
end
