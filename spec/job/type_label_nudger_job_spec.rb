# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/type_label_nudger_job'
require_relative '../../triage/triage/event'

RSpec.describe Triage::TypeLabelNudgerJob do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:project_id) { 999 }

    let(:event_attrs) do
      {
        action: 'open',
        from_gitlab_org?: true,
        iid: mr_iid,
        object_kind: 'merge_request',
        project_id: project_id,
        event_actor_username: 'bob'
      }
    end

    let(:mr_iid) { 1234 }
    let(:username) { 'requestor' }
  end

  # We don't reuse the one from spec/support/merge_request_notes_context.rb,
  # As calling `subject.unique_comment` will memoize a UniqueComment instance
  # with an event not defined (see the unique_comment method in TypeLabelNudgerJob)
  let(:comment_mark) do
    described_class.new.__send__(:unique_comment).__send__(:hidden_comment)
  end

  subject { described_class.new }

  describe '#perform' do
    let(:labels) { [] }
    let(:merge_request) do
      {
        project_id: project_id,
        iid: mr_iid,
        labels: labels
      }
    end
    let(:merge_request_discussions) do
      [id: 123, notes: merge_request_notes]
    end
    let(:merge_request_notes) { {} }

    before do
      stub_api_request(
        path: "/projects/#{project_id}/merge_requests/#{mr_iid}/discussions",
        query: { per_page: 100 },
        response_body: merge_request_discussions
      )
    end

    shared_examples 'not posting a reminder' do
      it 'does not post a reminder' do
        expect_api_request(path: "/projects/#{project_id}/merge_requests/#{mr_iid}", response_body: merge_request) do
          subject.perform(event)
        end
      end
    end

    context 'when at least one type label is present' do
      let(:labels) { ['type::bug'] }

      it_behaves_like 'not posting a reminder'
    end

    context 'when ~"Community contribution" label is present' do
      let(:labels) { ['Community contribution'] }

      it_behaves_like 'not posting a reminder'
    end

    context 'when no type labels are present' do
      let(:labels) { [] }

      context 'when no discussion from the processor is present' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: 'review discussion 2' },
          ]
        end

        it 'creates a discussion' do
          body = <<~MARKDOWN.chomp
            #{comment_mark}
            :wave: @#{event.event_actor_username} - please add ~"type::bug" ~"type::feature", ~"type::maintenance" or a [subtype](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification) label to this merge request. 

            - ~"type::bug": Defects in shipped code and fixes for those defects. This includes all the bug types (availability, performance, security vulnerability, mobile, etc.) 
            - ~"type::feature": Effort to deliver new features, feature changes & improvements. This includes all changes as part of new product requirements like application limits.
            - ~"type::maintenance": Up-keeping efforts & catch-up corrective improvements that are not Features nor Bugs. This includes restructuring for long-term maintainability, stability, reducing technical debt, improving the contributor experience, or upgrading dependencies.
    
            See [the handbook](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification) for more guidance on classifying.
          MARKDOWN

          expect_api_requests do |requests|
            requests << stub_api_request(path: "/projects/#{project_id}/merge_requests/#{mr_iid}", response_body: merge_request)
            requests << stub_discussion_request(event: event, body: body)

            subject.perform(event)
          end
        end
      end

      context 'when a discussion from the processor is present' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: "#{comment_mark} processor message", resolved: true }
          ]
        end

        it_behaves_like 'not posting a reminder'
      end
    end
  end
end
