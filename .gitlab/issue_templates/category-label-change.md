## Summary

This template is for new or renaming Category labels. This review helps to ensure there will be no impact with triage automation and reports.

### Action items

* [ ] Link the merge request to www-gitlab-com for the [category.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/categories.yml) change
* [ ] (If applicable) Label migration on existing issues and merge requests: apply the new label to opened & closed issues, and open & merged merge requests.
* [ ] (If applicable) Rename the old label by adding `[DEPRECATED]` at the end of the label name, like ~"CI/CD [DEPRECATED]".


/cc @gitlab-org/quality/engineering-productivity
/label ~Quality ~"label change"
