# frozen_string_literal: true

require 'csv'

module Triage
  class OrgByUsernameLocator
    class CsvUrlMissingError < StandardError
      def initialize(msg = "CSV Url is missing. The \"#{CSV_URL_VAR}\" Env variable needs to be declared")
        super
      end
    end

    class CsvUrlInvalidError < StandardError
      def initialize(msg = 'CSV Url is invalid.')
        super
      end
    end

    CSV_URL_VAR = 'USER_ACCOUNT_CSV_URL'
    CSV_URL_VALIDATION_HOST = 'app.periscopedata.com'
    CSV_URL_VALIDATION_STRING = 'api/gitlab'

    USER_ID_HEADER_NAME = 'USER_ID'
    ACCOUNT_ID_HEADER_NAME = 'CRM_ACCOUNT_ID'

    USER_ORG_CSV_CACHE_EXPIRY = 14400

    def self.locate_org(id)
      new.locate_org(id)
    end

    def locate_org(id)
      matching_row = user_account_csv_map.find { |row| row[USER_ID_HEADER_NAME] == id.to_s }
      matching_row&.fetch(ACCOUNT_ID_HEADER_NAME)
    end

    private

    def user_account_csv_map
      ::Triage.cache.get_or_set(:user_account_csv_map, expires_in: USER_ORG_CSV_CACHE_EXPIRY) do
        retrieve_csv.map(&:to_hash)
      end
    end

    def retrieve_csv
      body = HTTParty.get(csv_url)
      CSV.parse(body.parsed_response, headers: :first_row)
    end

    def csv_url
      ENV.fetch(CSV_URL_VAR).tap do |url|
        validate_url!(url)
      end
    end

    def validate_url!(url)
      uri = begin
        URI.parse(url)
      rescue URI::InvalidURIError
        raise CsvUrlMissingError.new
      end

      raise CsvUrlInvalidError.new('CSV Url is invalid: CSV Url is for the wrong host') unless uri.host == CSV_URL_VALIDATION_HOST
      raise CsvUrlInvalidError.new('CSV Url is invalid: CSV Url looks incorrect') unless url.include?(CSV_URL_VALIDATION_STRING)
    end
  end
end
