# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/job'
require_relative '../triage/reaction'
require_relative '../triage/unique_comment'
require_relative '../processor/type_label_nudger'

module Triage
  class TypeLabelNudgerJob < Job
    include Reaction

    private

    def execute(event)
      prepare_executing_with(event)

      return if community_contribution?
      return unless need_to_nudge?

      add_discussion(type_label_nudger_comment)
    end

    def community_contribution?
      merge_request.labels.include?(Triage::TypeLabelNudger::COMMUNITY_CONTRIBUTION_LABEL)
    end

    def need_to_nudge?
      !type_label_present? && !unique_comment.previous_discussion
    end

    def merge_request
      @merge_request ||= Triage.api_client.merge_request(event.project_id, event.iid)
    end

    def type_label_present?
      merge_request.labels.any? { |label| label.start_with?('type::', 'bug::', 'feature::', 'maintenance::') }
    end

    def type_label_nudger_comment
      comment = <<~MARKDOWN.chomp
        :wave: @#{event.event_actor_username} - please add ~"type::bug" ~"type::feature", ~"type::maintenance" or a [subtype](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification) label to this merge request. 

        - ~"type::bug": Defects in shipped code and fixes for those defects. This includes all the bug types (availability, performance, security vulnerability, mobile, etc.) 
        - ~"type::feature": Effort to deliver new features, feature changes & improvements. This includes all changes as part of new product requirements like application limits.
        - ~"type::maintenance": Up-keeping efforts & catch-up corrective improvements that are not Features nor Bugs. This includes restructuring for long-term maintainability, stability, reducing technical debt, improving the contributor experience, or upgrading dependencies.

        See [the handbook](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification) for more guidance on classifying.
      MARKDOWN

      unique_comment.wrap(comment).strip
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new('Triage::TypeLabelNudger', event)
    end
  end
end
