# frozen_string_literal: true

require 'digest'

require_relative '../../triage/processor'
require_relative '../../triage/rate_limit'

require_relative '../../../lib/www_gitlab_com'
require_relative '../../../lib/merge_request_coach_helper'

module Triage
  # Allows any community contributor to ask for a review.
  # The automation will pick a random MR coach and ask them to review the MR.
  class CommandMrRequestReview < Processor
    GITLAB_CHART_PROJECT_ID = 3828396

    include RateLimit
    include MergeRequestCoachHelper

    react_to 'merge_request.note'
    define_command name: 'request_review'

    def applicable?
      event.from_gitlab_org? &&
        event.by_noteable_author? &&
        event.wider_community_author? &&
        command.valid?(event)
    end

    def process
      coach = select_random_merge_request_coach
      comment = <<~MARKDOWN.rstrip
        Hey there #{coach}, can you please review this MR or find another reviewer?
        /assign_reviewer #{coach}
        #{quick_action_for_ready_for_review}
      MARKDOWN

      add_comment comment
    end

    private

    def quick_action_for_ready_for_review
      '/label ~"workflow::ready for review"' if from_gitlab_chart_project? && !draft_mr?
    end

    def draft_mr?
      event.payload.dig('merge_request', 'work_in_progress')
    end

    def from_gitlab_chart_project?
      event.with_project_id?(GITLAB_CHART_PROJECT_ID)
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("request_review-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      1
    end

    def rate_limit_period
      86400 # 1 day
    end
  end
end
