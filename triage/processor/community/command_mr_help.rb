# frozen_string_literal: true

require 'digest'

require_relative '../../triage/processor'
require_relative '../../triage/rate_limit'

require_relative '../../../lib/www_gitlab_com'
require_relative '../../../lib/merge_request_coach_helper'

module Triage
  class CommandMrHelp < Processor
    include RateLimit
    include MergeRequestCoachHelper

    react_to 'merge_request.note'
    define_command name: 'help'

    def applicable?
      event.from_gitlab_org? &&
        event.by_noteable_author? &&
        event.wider_community_author? &&
        command.valid?(event)
    end

    def process
      coach = select_random_merge_request_coach
      add_comment <<~MARKDOWN.chomp
        Hey there #{coach}, could you please help @#{event.event_actor_username} out?
        /assign_reviewer #{coach}
      MARKDOWN
    end

    private

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("help-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      1
    end

    def rate_limit_period
      86400 # 1 day
    end
  end
end
