# frozen_string_literal: true

require 'digest'
require 'slack-messenger'

require_relative '../../triage'
require_relative '../../triage/processor'
require_relative '../../triage/rate_limit'

module Triage
  class CommandMrFeedback < Processor
    include RateLimit

    SLACK_CHANNEL = '#mr-feedback'
    SLACK_ICON = ':robot_face:'
    SLACK_MESSAGE_TEMPLATE = <<~MESSAGE
      Hola MR coaches, a contributor has left feedback about their experience in %<comment_url>s.
    MESSAGE

    react_to 'merge_request.note'
    define_command name: 'feedback'

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      event.from_gitlab_org? &&
        event.by_noteable_author? &&
        event.wider_community_author? &&
        command.valid?(event)
    end

    def process
      post_mr_feedback_request
    end

    def slack_options
      {
        channel: SLACK_CHANNEL,
        username: GITLAB_BOT,
        icon_emoji: SLACK_ICON
      }
    end

    private

    attr_reader :messenger

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("feedback-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      1
    end

    def rate_limit_period
      86400 # 1 day
    end

    def post_mr_feedback_request
      message = format(SLACK_MESSAGE_TEMPLATE, comment_url: event.url)
      messenger.ping(message)
    end

    def slack_messenger
      Slack::Messenger.new(ENV['SLACK_WEBHOOK_URL'], slack_options)
    end
  end
end
