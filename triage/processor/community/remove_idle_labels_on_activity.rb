# frozen_string_literal: true

require_relative '../../triage/processor'

module Triage
  class RemoveIdleLabelOnActivity < Processor
    COMMUNITY_CONTRIBUTION_LABEL = 'Community contribution'
    IDLE_LABEL = 'idle'
    STALE_LABEL = 'stale'

    react_to 'merge_request.note'

    def applicable?
      event.from_gitlab_org? &&
        !event.gitlab_bot_event_actor? &&
        community_contribution? &&
        idle_or_stale?
    end

    def process
      add_comment <<~COMMENT.chomp
        /unlabel ~"#{IDLE_LABEL}" ~"#{STALE_LABEL}"
      COMMENT
    end

    private

    def label_names
      @label_names ||= event.label_names
    end

    def community_contribution?
      label_names.include?(COMMUNITY_CONTRIBUTION_LABEL)
    end

    def idle_or_stale?
      (label_names & [IDLE_LABEL, STALE_LABEL]).any?
    end
  end
end
