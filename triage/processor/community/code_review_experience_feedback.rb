# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../triage/unique_comment'

module Triage
  class CodeReviewExperienceFeedback < Processor

    react_to 'merge_request.merge', 'merge_request.close'

    def applicable?
      (
        (event.from_gitlab_org? && event.wider_community_author?) ||
        (event.from_gitlab_com? && event.wider_gitlab_com_community_author?)
      ) && unique_comment.no_previous_comment?
    end

    def process
      post_review_experience_comment
    end

    private

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end

    def post_review_experience_comment
      add_comment(message.strip)
    end

    def message
      comment =  <<~MARKDOWN.chomp
        @#{event.resource_author.username}, how was your code review experience with this merge request? Please tell us how we can continue to [iterate](https://about.gitlab.com/handbook/values/) and improve:

        1. Leave a :thumbsup: or a :thumbsdown: on this comment to describe your experience.
        1. Create a new comment starting with `@gitlab-bot feedback` below, and leave any additional feedback you have for us in the comment.
        
        Have five minutes? Take our [survey](https://forms.gle/g26h8uEKgvTLGSQw6) to give us even more feedback on how GitLab can improve the contributor experience.

        Thanks for your help! :heart:
      MARKDOWN

      unique_comment.wrap(comment)
    end
  end
end
