# frozen_string_literal: true

require_relative '../../triage/processor'

module Triage
  class RemoveWorkflowInDevLabelOnAuthorActivity < Processor
    LEGACY_PINGPONG_LABEL = "🏓"
    WORKFLOW_IN_DEV_LABEL = "workflow::in dev"

    react_to 'merge_request.note'

    def applicable?
      event.from_gitlab_org? &&
        event.by_noteable_author? &&
        (
          event.label_names.include?(WORKFLOW_IN_DEV_LABEL) ||
          event.label_names.include?(LEGACY_PINGPONG_LABEL) # back-compatibility support, until all MRs are migrated to use `workflow::in dev`
        )
    end

    def process
      add_comment(%Q{/unlabel ~"#{WORKFLOW_IN_DEV_LABEL}" ~"#{LEGACY_PINGPONG_LABEL}"})
    end
  end
end
