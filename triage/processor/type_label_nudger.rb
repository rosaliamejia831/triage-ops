# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative '../job/type_label_nudger_job'

module Triage
  # Reminds the MR creator to add a work type label to the MR.
  class TypeLabelNudger < Processor
    FIVE_MINUTES                 = 300
    COMMUNITY_CONTRIBUTION_LABEL = 'Community contribution'

    # List coming from https://app.periscopedata.com/app/gitlab/1021219/Type-Research?widget=14627996&udv=0
    #
    # Verify this list:
    #   curl --silent 'https://gitlab.com/api/v4/projects/<project_id>' | jq .path_with_namespace
    ALLOWED_PROJECTS_IDS = [
      '278964',       # gitlab-org/gitlab
      '1148549',      # gitlab-com/runbooks
      '7071551',      # gitlab-org/gitlab-ui
      '2009901',      # gitlab-org/gitaly
      '4456656',      # gitlab-org/gitlab-services/design.gitlab.com
      '26392061',     # gitlab-com/gl-infra/customersdot-ansible
      '430285',       # gitlab-org/release-tools
      '20699',        # gitlab-org/omnibus-gitlab
      '2670515',      # gitlab-org/customers-gitlab-com
      '1794617',      # gitlab-org/gitlab-docs
      '32149347',     # gitlab-org/opstrace/opstrace
      '5964710',      # gitlab-org/security-products/analyzers/api-fuzzing-src
      '12006272',     # gitlab-org/security-products/gemnasium-db
      '7776928',      # gitlab-org/quality/triage-ops
      '13831684',     # gitlab-org/container-registry
      '734943',       # gitlab-org/gitlab-pages
      '24631368',     # gitlab-org/modelops/anti-abuse/pipeline-validation-service
      '4359271',      # gitlab-org/build/CNG
      '3828396',      # gitlab-org/charts/gitlab
      '14022'         # gitlab-org/gitlab-shell
    ].freeze

    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      from_allowed_project? &&
        !community_contribution? &&
        (need_to_nudge? || need_to_resolve_nudge?)
    end

    # We initially give the author a few minutes to add a work type label before we post a reminder.
    #
    # Resolving/unresolving discussions, on the other hand, is instantaneous.
    def process
      if need_to_nudge?
        if unique_comment.previous_discussion
          unresolve_discussion(unique_comment.previous_discussion['id'])
        else
          TypeLabelNudgerJob.perform_in(FIVE_MINUTES, event)
        end
      elsif need_to_resolve_nudge?
        resolve_discussion(unique_comment.previous_discussion['id'])
      end
    end

    private

    def from_allowed_project?
      ALLOWED_PROJECTS_IDS.any? { |project_id| event.with_project_id?(project_id) }
    end

    def community_contribution?
      event.label_names.include?(COMMUNITY_CONTRIBUTION_LABEL)
    end

    def need_to_nudge?
      !type_label_present? &&
        (!unique_comment.previous_discussion_comment || unique_comment.previous_discussion_comment['resolved'])
    end

    def need_to_resolve_nudge?
      type_label_present? &&
        unique_comment.previous_discussion_comment &&
        !unique_comment.previous_discussion_comment['resolved']
    end

    def type_label_present?
      event.label_names.any? { |label| label.start_with?('type::', 'bug::', 'feature::', 'maintenance::') }
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end
  end
end
