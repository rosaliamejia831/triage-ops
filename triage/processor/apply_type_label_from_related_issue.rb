# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/related_issue_finder'

module Triage
  # This processor applies type label to an MR from a detected related issue.
  class ApplyTypeLabelFromRelatedIssue < Processor
    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      event.from_gitlab_org? &&
        !event.type_label_set? &&
        related_issue
    end

    def process
      apply_type_label_from_related_issue
    end

    private

    def related_issue
      @related_issue ||= Triage::RelatedIssueFinder.new.find_project_issue_in(event.project_id, event.description)
    end

    def type_label
      @type_label ||= related_issue&.labels.grep(/type::/).first
    end

    def apply_type_label_from_related_issue
      return unless type_label

      add_comment <<~MARKDOWN.chomp
        /label ~"#{type_label}"
      MARKDOWN
    end
  end
end
