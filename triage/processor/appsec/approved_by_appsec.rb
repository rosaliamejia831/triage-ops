# frozen_string_literal: true

require_relative '../appsec_processor'

module Triage
  class ApprovedByAppSec < AppSecProcessor
    react_to_approvals

    def applicable?
      super && approved_by_appsec?
    end

    def process
      discussion_id = add_to_appsec_thread(message)

      resolve_discussion(discussion_id)
    end

    private

    def message
      @message ||= unique_comment.wrap(<<~MARKDOWN.strip)
        :white_check_mark: AppSec approved for #{event.last_commit_sha}
        by `@#{event.event_actor_username}`, adding ~"#{APPSEC_APPROVAL_LABEL}"

        /label ~"#{APPSEC_APPROVAL_LABEL}"
      MARKDOWN
    end
  end
end
