# frozen_string_literal: true

require 'cgi'

class LazyHeatMap
  PRIORITY_LABELS = %w[priority::1 priority::2 priority::3 priority::4].freeze
  SEVERITY_LABELS = %w[severity::1 severity::2 severity::3 severity::4].freeze
  WITHOUT_PRIORITY_STRING = 'No priority'.freeze
  WITHOUT_SEVERITY_STRING = 'No severity'.freeze
  PRIORITY_TITLES = [*PRIORITY_LABELS, WITHOUT_PRIORITY_STRING].freeze
  SEVERITY_TITLES = [*SEVERITY_LABELS, WITHOUT_SEVERITY_STRING].freeze

  def initialize(resources, policy_spec, network)
    @resources = resources
    @policy_spec = policy_spec
    @network = network
    @options = network.options
  end

  def to_s
    @to_s ||= generate_heat_map_table
  end

  # With https://gitlab.com/gitlab-org/gitlab-triage/-/issues/255, we wouldn't need to pass options.
  def generate_heat_map_table(options = {})
    severities = heat_map.each_value.flat_map(&:keys).uniq

    body = heat_map.each_key.map do |priority|
      row = [header_string(priority)] +
        severities.map do |severity|
          issues_link(priority, severity, options)
        end

      "| #{row.join(' | ')} |"
    end.join("\n")

    header = "|| #{severities.map { |s| header_string(s) }.join(' | ')} |"
    separator = "|----|#{severities.map { '----' }.join('|')}|"

    "#{header}\n#{separator}\n#{body}"
  end

  private

  def heat_map
    @heat_map ||= generate_heat_map
  end

  def generate_heat_map
    grouped_by_priority = @resources.group_by do |resource|
      # Pick highest one
      resource[:labels].grep(/\Apriority::\d\z/).sort.first || WITHOUT_PRIORITY_STRING
    end

    grouped_by_priority = pad_and_sort(grouped_by_priority, PRIORITY_TITLES)

    grouped_by_priority.transform_values do |with_same_priority|
      grouped_by_severity = with_same_priority.group_by do |resource|
        resource[:labels].grep(/\Aseverity::\d\z/).sort.first || WITHOUT_SEVERITY_STRING
      end

      pad_and_sort(grouped_by_severity, SEVERITY_TITLES)
    end
  end

  def pad_and_sort(hash, labels)
    labels.each do |name|
      hash[name] ||= {}
    end

    hash.sort_by { |k, _| k[/\d/] || 'Z' }.to_h
  end

  # With https://gitlab.com/gitlab-org/gitlab-triage/-/issues/255, we wouldn't need to pass options.
  def issues_link(priority, severity, options = {})
    count = heat_map.dig(priority, severity)&.size || 0

    if count.zero?
      count
    else
      <<~MARKDOWN.chomp
        [#{count}](#{issues_base_url(options)}?#{issues_query(priority, severity)})
      MARKDOWN
    end
  end

  def issues_query(priority, severity)
    state = [@policy_spec.dig(:conditions, :state)].compact
    labels = @policy_spec.dig(:conditions, :labels) || []
    forbidden_labels = @policy_spec.dig(:conditions, :forbidden_labels) || []

    {
      'state' => state,
      'label_name[]' => labels + with_sp_labels(priority, severity),
      'not[label_name][]' => forbidden_labels + without_sp_labels(priority, severity)
    }.flat_map do |key, values|
      values.map { |v| "#{CGI.escape(key)}=#{CGI.escape(v)}" }
    end.join('&')
  end

  def issues_base_url(options = {})
    return if @resources.empty?

    # With https://gitlab.com/gitlab-org/gitlab-triage/-/issues/255, we could use
    # `@policy_spec.dig(:limits, :project)` instead of passing options.
    source_url_for_resource(@resources.first, project_id: options[:project_id])
  end

  def header_string(header_label)
    if [WITHOUT_SEVERITY_STRING, WITHOUT_PRIORITY_STRING].include?(header_label)
      header_label
    else
      %Q{~"#{header_label}"}
    end
  end

  def with_sp_labels(priority, severity)
    [
      *(priority unless priority == WITHOUT_PRIORITY_STRING),
      *(severity unless severity == WITHOUT_SEVERITY_STRING)
    ]
  end

  def without_sp_labels(priority, severity)
    [
      *(PRIORITY_LABELS if priority == WITHOUT_PRIORITY_STRING),
      *(SEVERITY_LABELS if severity == WITHOUT_SEVERITY_STRING)
    ]
  end

  def source_url_for_resource(resource, project_id: nil)
    context = Gitlab::Triage::Resource::Context.build(
      resource, network: @network)

    resource_type = context.class.name.demodulize.underscore.pluralize

    source_id = project_id || @options.source_id
    source =
      if project_id || @options.source == :projects
        context.__send__(:request_project, source_id)
      else
        context.__send__(:request_group, source_id)
      end

    "#{source[:web_url]}/-/#{resource_type}"
  end
end

module IssueBuilderWithHeatMap
  private

  def description_resource
    super.merge(heat_map: LazyHeatMap.new(@resources, @policy_spec, @network))
  end
end
