# frozen_string_literal: true

require_relative 'group_definition'

module GroupTriageHelperContext
  REPORT_DUE = 'in 7 days'
  REPORT_LABELS = ['triage report'].freeze

  include GroupDefinition

  def merge_requests_needing_attention_report(assignees: [], labels: [], mentions: assignees, **args)
    <<~SUMMARY
      Hi, #{build_mentions(mentions)}

      This is a group level merge requests report. This report contains:

      - Open team MRs updated more than 28 days ago

      > Please review these merge requests to identify if there are any steps that can shorten the time to merge, such as reminding the author or reviewers about it, marking it as draft or closing the MR.

      Weekly idle merge request trends by group can be found in the [Sisense dashboard](https://app.periscopedata.com/app/gitlab/663398) or details on the report in [the handbook](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#group-level-merge-requests-that-may-need-attention)

      - [ ] Confirm your group's Open MR Review Time is within target on your [Engineering metrics dashboard](https://about.gitlab.com/handbook/engineering/metrics/#engineering-metrics-dashboards)
      ----

      {{items}}

      ----

      Note: Using full links to the merge requests will update the merge request
      which will remove them from the future report.

      /label #{build_labels(labels)}
      /label #{build_labels(REPORT_LABELS)}
      /assign #{build_mentions(assignees)}
    SUMMARY
  end

  def report_title(group_name)
     "#{Date.today.iso8601} - Triage report for \"#{group_name}\" - #{source_name(network.options.source, network.options.source_id)}"
  end

  def source_name(type, path)
    case type
    when :projects
      request_project(path)[:path_with_namespace]
    else
      request_group(path)[:full_path]
    end
  end

  def report_summary(assignees: [], labels: [], mentions: assignees, **args)
    <<~SUMMARY
      Hi, #{build_mentions(mentions)}

      This is a group or stage level triage report that aims to summarize
      the feature proposals and bugs which have not been scheduled or triaged.
      For more information please refer to the handbook:
      - https://about.gitlab.com/handbook/engineering/quality/triage-operations/index.html#triage-reports

      Scheduling the workload is a collaborative effort by the Product Managers
      and Engineering Managers for that group. Please work together to provide
      a best estimate on priority and milestone assignments. For each issue please:

      - Determine if the issue should be closed if it is no longer relevant or a
      duplicate.
      - If it is still relevant please assign either a best estimate versioned
      milestone, the %Backlog or the %"Awaiting further demand" milestone.
      - Specifically for ~bug, if there is no priority or clarity on a versioned
      milestone, please add a Priority label. Priority labels have an estimate
      SLO attached to them and help team members and the wider community understand
      roughly when it will be considered to be scheduled.
        - https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#priority-labels
      - Once a milestone has been assigned please check off the box for that issue.
      - Please work with your team to complete the list by the due date set.

      {{items}}

      /label #{build_labels(labels)}
      /label #{build_labels(REPORT_LABELS)}
      /due #{REPORT_DUE}
      /assign #{build_mentions(assignees)}

      _This report has been set to confidential as it may contain information about the priority and severity of security issues in each group._

      /confidential

      ---

      This is a group level triage report that aims to collate the latest bug reports (for frontend and otherwise) and feature proposals.
      For more information please refer to the handbook:
      - https://about.gitlab.com/handbook/engineering/quality/triage-operations/index.html#triage-reports

      ---

      If assignees or people mentioned in this individual triage report need to be amended, please edit [group-definition.yml](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/group-definition.yml).

    SUMMARY
  end

  def short_team_summary(title: '', assignees: [], labels: [], mentions: assignees, **args)
    <<~SUMMARY
      Hi, #{build_mentions(mentions)}

      #{title}

      {{items}}

      /label #{build_labels(labels)}
      /label #{build_labels(REPORT_LABELS)}
      /due #{REPORT_DUE}
      /assign #{build_mentions(assignees)}
    SUMMARY
  end

  def feature_proposal_intro
    <<~INTRO
      ### Feature Proposal Section

      For the following feature proposals. Please either close or assign either a versioned milestone, the %Backlog or the %"Awaiting further demand" milestone.

    INTRO
  end

  def security_section_intro
    <<~INTRO
      ### Security Section

    INTRO
  end

  def security_heatmap
    paragraph = <<~HEATMAP_PARAGRAPH
      Security vulnerability issues for their priority and severity label are counted here. Please take a look at the issues
      which fall into the columns indicating that the priority or severity labels
      are currently missing.

      Please see the [Security prioritization guidelines](https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues) for more detail.
    HEATMAP_PARAGRAPH

    titled_heatmap("#### Heatmap for security vulnerability issues", paragraph: paragraph)
  end

  def availability_section_intro
    <<~INTRO
      ### Availability Section

    INTRO
  end

  def availability_heatmap
    paragraph = <<~HEATMAP_PARAGRAPH
      Availability issues for their priority and severity label are counted here. Please take a look at the issues
      which fall into the columns indicating that the priority or severity labels
      are currently missing.

      Please see the [Availability prioritization guidelines](https://about.gitlab.com/handbook/engineering/performance/#availability) for more detail.
    HEATMAP_PARAGRAPH

    titled_heatmap("#### Heatmap for all availability issues", paragraph: paragraph)
  end

  def infradev_section_intro
    <<~INTRO
      ### Infradev Section

    INTRO
  end

  def infradev_latest_issues_summary
    titled_summary("#### Infradev issues new this week")
  end

  def infradev_all_issues_heatmap
     titled_heatmap("### Heatmap for all ~infradev issues")
  end

  def infradev_overdue_issues_summary
    summary = <<~SUMMARY
      #### Infradev issues past SLO

      This section highlights ~infradev issues that have an assigned milestone with a due date past the threshold of their SLO based on their current severity label

      For example,
        - ~"severity::1" issue was assigned that label 10 days ago
        - The assigned milestone is due in 30 days

      Based on the current SLO for ~"severity::1" (30 days), it would be delivered 10 days past the threshold
    SUMMARY

    titled_summary(summary)
  end

  def bug_section_intro
    <<~INTRO
      ### Bug Section

      For the following bugs. Please either close or assign either a versioned
      milestone, the %Backlog or the %"Awaiting further demand" milestone and
      ensure that a priority label is set.

      - Engineering Managers: Please add a severity label for those issues without one
      - Product Designers: Please add a severity label to ~UX ~bug issues without one

    INTRO
  end

  def bug_heatmap
    paragraph = <<~HEATMAP_PARAGRAPH
      Bugs for their priority and severity label are counted here. Every bug should
      have severity and priority labels applied. Please take a look at the bugs
      which fall into the columns indicating that the priority or severity labels
      are currently missing.
    HEATMAP_PARAGRAPH

    titled_heatmap("#### Heatmap for all bugs", paragraph: paragraph)
  end

  def missed_slo_heatmap
    titled_heatmap("### Heatmap for ~missed-SLO bugs")
  end

  def vintage_heatmap
    titled_heatmap("### Heatmap for ~vintage bugs")
  end

  def customer_bugs_heatmap
    titled_heatmap("### Heatmap for ~customer bugs")
  end

  def unscheduled_feature_summary
    titled_summary("#### Unscheduled ~feature (non-customer)")
  end

  def unscheduled_customer_feature_summary
    titled_summary("#### Unscheduled ~feature with ~customer")
  end

  def unscheduled_ux_debt_summary
    titled_summary("#### Unscheduled UX Debt Issues")
  end

  def ux_debt_section_intro
    <<~INTRO
      ### UX debt Section

      For the following UX debt issues. Please either close or assign either a versioned
      milestone, the %Backlog or the %"Awaiting further demand" milestone and
      ensure that a priority label is set.

    INTRO
  end

  def ux_debt_heatmap
    paragraph = <<~HEATMAP_PARAGRAPH
      UX debt issues for their priority and severity label are counted here. Every UX debt issue should
      have severity and priority labels applied. Please take a look at the UX debt issues
      which fall into the columns indicating that the priority or severity labels
      are currently missing.
    HEATMAP_PARAGRAPH

    titled_heatmap("#### Heatmap for all UX debt", paragraph: paragraph)
  end

  def unscheduled_ui_polish_summary
    titled_summary("#### Unscheduled UI Polish Issues")
  end

  def ui_polish_section_intro
    <<~INTRO
      ### UI polish Section

      For the following UI polish issues. Please either close or assign either a versioned
      milestone, the %Backlog or the %"Awaiting further demand" milestone and
      ensure that a priority label is set.

    INTRO
  end

  def ui_polish_heatmap
    paragraph = <<~HEATMAP_PARAGRAPH
      UI polish issues for their priority and severity label are counted here. Every UI polish issue should
      have severity and priority labels applied. Please take a look at the UI polish issues
      which fall into the columns indicating that the priority or severity labels
      are currently missing.
    HEATMAP_PARAGRAPH

    titled_heatmap("#### Heatmap for all UI polish", paragraph: paragraph)
  end

  def remaining_unscheduled_bugs_summary
    titled_summary("#### Unscheduled ~bug (non-customer)")
  end

  def remaining_unscheduled_customer_bugs_summary
    titled_summary("#### Unscheduled ~bug with ~customer")
  end

  def frontend_unscheduled_bugs_summary
    titled_summary("#### Unscheduled ~frontend ~bug (non-customer)")
  end

  def frontend_unscheduled_customer_bugs_summary
    titled_summary("#### Unscheduled ~frontend ~bug with ~customer")
  end

  def quarantined_flaky_specs_summary
    titled_summary('#### Open Quarantined Flaky Specs Issues')
  end

  def quality_report_summary(title, items)
    first_item = items.lines.first
    section_label = first_item.match(/section::\w+/)&.to_s
    severity_label = first_item.match(/severity::\d+/)&.to_s
    markdown_labels = [section_label, severity_label].compact.map { |label| %Q(~"#{label}") }
    collapsible_titled_summary([title, *markdown_labels].join(' '))
  end

  def titled_heatmap(title, options = {})
    <<~HEATMAP
      #{title}

      #{options[:paragraph]}

      #{resource[:heat_map].generate_heat_map_table(options)}

      ----
    HEATMAP
  end

  def collapsible_titled_summary(title)
    <<~SUMMARY

    <details>

    <summary markdown="span"><b>#{title}</b></summary>

    {{items}}

    </details>

    SUMMARY
  end

  def titled_summary(title)
    <<~SUMMARY

    #{title}

    {{items}}

    ----
    SUMMARY
  end

  def build_mentions(usernames)
    build_command(usernames.map(&:strip), prefix: '@')
  end

  def build_labels(labels)
    build_command(labels.map(&:strip), prefix: '~', quote: true)
  end

  def build_command(strings, prefix: '', quote: false)
    strings.map do |string|
      body =
        if quote
          %Q{"#{string}"}
        else
          string
        end

      if string.start_with?(prefix)
        body
      else
        "#{prefix}#{body}"
      end
    end.join(' ')
  end
end
