# frozen_string_literal: true

require_relative '../lib/merge_request_coach_helper'

Gitlab::Triage::Resource::Context.include MergeRequestCoachHelper
