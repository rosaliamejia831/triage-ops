---
# Across different groups and projects, defined in the jobs
default-labels:
  variants:
    - description: '[DAILY] defaults labels for groups and projects'
      cron: '0 0 * * *' # At 00:00.
      variables:
        # **DO NOT** DEFINE TRIAGE_SOURCE_PATH for this schedule
        # We're relying on the jobs here.
        # pre-hygiene
        DEFAULT_LABELING: 1
        # hygiene
        TRIAGE_STAGE_AND_GROUP_LABELS_HYGIENE: 1
        # Threat Insights
        TRIAGE_THREAT_INSIGHTS_WEIGHT_HYGIENE: 1
        TRIAGE_THREAT_INSIGHTS_REFINEMENT_HYGIENE: 1
        TRIAGE_THREAT_INSIGHTS_VERIFICATION_HYGIENE: 1
        TRIAGE_THREAT_INSIGHTS_TECH_STACK_LABEL: 1
        TRIAGE_THREAT_INSIGHTS_REMOVE_ASSIGNEE: 1
        # Container Security
        TRIAGE_CONTAINER_SECURITY_WEIGHT_HYGIENE: 1
        TRIAGE_CONTAINER_SECURITY_REFINEMENT_HYGIENE: 1
        TRIAGE_CONTAINER_SECURITY_VERIFICATION_HYGIENE: 1
        TRIAGE_CONTAINER_SECURITY_REMOVE_ASSIGNEE: 1
        # DAST
        TRIAGE_DAST_REFINEMENT_HYGIENE: 1
        # Editor
        TRIAGE_EDITOR_TECH_STACK_LABEL: 1
        TRIAGE_EDITOR_VERIFICATION_HYGIENE: 1
        # Global Search
        TRIAGE_GLOBAL_SEARCH_WEIGHT_HYGIENE: 1
        # Plan
        TRIAGE_CERTIFY_GROUP_LABEL_HYGIENE: 1

missed-resources:
  variants:
    - description: '[DAILY] Add missed labels for projects'
      cron: '5 0 * * *' # At 00:05.
      cron_timezone: America/Los_Angeles
      variables:
        TZ: America/Los_Angeles
        # **DO NOT** DEFINE TRIAGE_SOURCE_PATH for this schedule
        # We're relying on the jobs here.
        TRIAGE_MISSED_RESOURCES: 1

# Groups
gitlab-org/charts:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: groups
      TRIAGE_SOURCE_PATH: 5032027
  variants:
    - description: '[DAILY] gitlab-org/charts'
      cron: '0 1 * * *' # At 01:00.
      variables:
        # hygiene
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1

gitlab-org/distribution:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: groups
      TRIAGE_SOURCE_PATH: 2639717
  variants:
    - description: '[DAILY] gitlab-org/distribution'
      cron: '0 1 * * *' # At 01:00.
      variables:
        # hygiene
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1

gitlab-org:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: groups
      TRIAGE_SOURCE_PATH: 9970
  variants:
    - description: '[HOURLY] gitlab-org label community contributions'
      cron: '0 0 * * *' # At 00:00.
      variables:
        # hygiene
        TRIAGE_LABEL_COMMUNITY_CONTRIBUTIONS: 1
    - description: '[DAILY] gitlab-org hygiene'
      cron: '0 0 * * *' # At 00:00.
      variables:
        # hygiene
        TRIAGE_ASK_SEVERITY_PRIORITY_FOR_INFRADEV_ISSUES: 1
        TRIAGE_ASK_SEVERITY_FOR_SUS_IMPACTING_ISSUES: 1
        TRIAGE_DISCOVER: 1
        TRIAGE_LABEL_AVAILABILITY_ISSUES: 1
        TRIAGE_LABEL_SUS_IMPACTING_ISSUES: 1
        TRIAGE_LABEL_JIHU_CONTRIBUTIONS: 1
        TRIAGE_LABEL_BOT_MRS: 1
        TRIAGE_ADD_LEGAL_DISCLAIMER_TO_DIRECTION_EPICS: 1
        TRIAGE_LABEL_IDLE_COMMUNITY_MRS: 1
        TRIAGE_CLOSE_TEST_FAILURE_ISSUES: 1
    - description: '[DAILY] gitlab-org reports'
      cron: '0 2 * * 1-5' # At 02:00 on every day-of-week from Monday through Friday.
      variables:
        # report
        TRIAGE_UNTRIAGED_COMMUNITY_MERGE_REQUESTS_REPORT: 1
        # close-reports
        TRIAGE_CLOSE_OLD_TRIAGE_REPORTS: 1
        TRIAGE_CLOSE_OLD_FLAKY_EXAMPLES_REPORT: 1
        TRIAGE_CLOSE_REPORTS_RESOURCES_MOVED_TO_NEXT_MILESTONE: 1
    - description: '[WEEKLY] gitlab-org'
      cron: '0 3 * * 1' # At 03:00 on Monday.
      variables:
        # report
        TRIAGE_EP_REPORT: 1
        QUAD_PLANNING_REPORT: 1
        TRIAGE_MISSING_CATEGORIES: 1
        TRIAGE_COMMUNITY_REPORT: 1
        TRIAGE_CLOSE_STALE_BUGS: 1
        TRIAGE_RUNNER_GROUP_SUMMARY: 1
        TRIAGE_SECURITY_PRODUCTS_RECENTLY_DELIVERED: 1
        TRIAGE_NUDGE_STALE_COMMUNITY_CONTRIBUTIONS: 1
    - description: '[MONTHLY] gitlab-org prompt for tier labels'
      cron: '0 4 23 * *' # At 04:00 on day-of-month 23.
      variables:
        # hygiene
        TRIAGE_PROMPT_FOR_TIER_LABELS: 1
    - description: '[On 8th and 23rd] Configure milestone check & Quality report'
      cron: '0 2 8,23 * *' # At 02:00 on day-of-month 8 and 23.
      variables:
        TRIAGE_CONFIGURE_MILESTONE_CHECK: 1
        TRIAGE_QUALITY_REPORT: 1

# Projects
gitlab-org/quality/triage-ops:
  base:
    active: true
  variants:
    - description: '[6-hourly] gitlab-org/quality/triage-ops regular pipeline run'
      cron: '0 */6 * * *' # At minute 0 past every 6th hour.
      variables:
        SCHEDULED_TESTS: 1

gitlab-com/www-gitlab-com:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 7764
  variants:
    - description: '[DAILY] gitlab-com/www-gitlab-com'
      variables:
        # hygiene
        TRIAGE_WEBSITE_LABEL_COMMUNITY_CONTRIBUTIONS: 1

gitlab-org/customers-gitlab-com:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 2670515
  variants:
    - description: '[DAILY] gitlab-org/customers-gitlab-com'
      variables:
        # hygiene
        TRIAGE_CLOSE_OLD_ISSUES: 1

gitlab-org/gitaly:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 2009901
  variants:
    - description: '[DAILY] gitlab-org/gitaly'
      cron: '0 1 * * *' # At 01:00.
      variables:
        # hygiene
        TRIAGE_LABEL_MISSED_SLO: 1
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_LABEL_SEEKING_COMMUNITY_CONTRIBUTIONS: 1
        TRIAGE_LABEL_VULNERABILITY: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1
    - description: '[WEEKLY] gitlab-org/gitaly'
      cron: '0 2 * * 1' # At 02:00 on Monday.
      variables:
        # report
        TRIAGE_TEAM_SUMMARY: 1
    - cron: '0 2 8,23 * *' # At 02:00 on day-of-month 8 and 23.
      description: '[On 8th and 23nd] gitlab-org/gitaly'
      variables:
        TRIAGE_SOURCE_PATH: gitlab-org/gitaly
        # report
        TRIAGE_MERGE_REQUESTS_NEEDING_ATTENTION: 1

gitlab-org/gitlab-foss:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 13083
  variants:
    - description: '[3-hour] gitlab-org/gitlab-foss migrate open gitlab-foss issues to gitlab'
      cron: '0 */3 * * *' # At minute 0 past every 3rd hour.
      variables:
        # hygiene
        TRIAGE_GITLAB_FOSS_MIGRATE_ISSUES_TO_GITLAB: 1

gitlab-org/gitlab-qa:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 1441932
  variants:
    - description: '[DAILY] gitlab-org/gitlab-qa'
      cron: '0 1 * * *' # At 01:00.
      variables:
        # hygiene
        TRIAGE_LABEL_MISSED_SLO: 1
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_LABEL_SEEKING_COMMUNITY_CONTRIBUTIONS: 1
        TRIAGE_LABEL_VULNERABILITY: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1
    - cron: '0 2 8,23 * *' # At 02:00 on day-of-month 8 and 23.
      description: '[On 8th and 23nd] gitlab-org/gitlab-qa'
      variables:
        TRIAGE_SOURCE_PATH: gitlab-org/gitlab-qa
        # report
        TRIAGE_MERGE_REQUESTS_NEEDING_ATTENTION: 1

gitlab-org/gitlab-runner:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 250833
  variants:
    - description: '[DAILY] gitlab-org/gitlab-runner'
      cron: '0 1 * * *' # At 01:00.
      variables:
        # hygiene
        TRIAGE_LABEL_MISSED_SLO: 1
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_LABEL_SEEKING_COMMUNITY_CONTRIBUTIONS: 1
        TRIAGE_LABEL_VULNERABILITY: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1
    - description: '[WEEKLY] gitlab-org/gitlab-runner'
      cron: '0 2 * * 1' # At 02:00 on Monday.
      variables:
        # report
        TRIAGE_TEAM_SUMMARY: 1
    - cron: '0 2 8,23 * *' # At 02:00 on day-of-month 8 and 23.
      description: '[On 8th and 23nd] gitlab-org/gitlab-runner'
      variables:
        TRIAGE_SOURCE_PATH: gitlab-org/gitlab-runner
        # report
        TRIAGE_MERGE_REQUESTS_NEEDING_ATTENTION: 1

gitlab-org/gitlab:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 278964
  variants:
    - description: '[DAILY] gitlab-org/gitlab'
      cron: '0 1 * * *' # At 01:00.
      variables:
        # hygiene
        TRIAGE_ADD_MILESTONE_TO_COMMUNITY_MERGE_REQUESTS: 1
        TRIAGE_LABEL_ESTIMATION_COMPLETED: 1
        TRIAGE_LABEL_MISSED_SLO: 1
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_LABEL_SEEKING_COMMUNITY_CONTRIBUTIONS: 1
        TRIAGE_LABEL_VULNERABILITY: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1
        TRIAGE_SET_PRIORITY_FROM_SEVERITY: 1
        TRIAGE_WARN_IMPENDING_SLO_BREACH: 1
        PROMPT_TEAM_MEMBER_TYPE_LABEL: 1
        MANAGE_ACCESS_FEATURE_FLAG_CHECK: 1
        PROMPT_FOR_VULNERABILITY_LABEL: 1
    - cron: '0 2 * * 1-5' # At 02:00 on every day-of-week from Monday through Friday.
      description: '[DAILY-WEEKDAY] gitlab-org/gitlab untriaged report'
      variables:
        # report
        TRIAGE_UNTRIAGED_ISSUES: 1
    - cron: '0 2 * * 1' # At 02:00 on Monday.
      description: '[WEEKLY] gitlab-org/gitlab'
      variables:
        # report
        TRIAGE_TEAM_SUMMARY: 1
        TRIAGE_FLAKY_EXAMPLES_REPORT: 1
    - cron: '0 5 8,15 * *' # At 05:00 on day-of-month 8 and 15.
      description: '[On 8th and 15th] gitlab-org/gitlab issues slipping hygiene'
      variables:
        PROMPT_FOR_SLIPPING_TRIAGE: 1
    - cron: '0 5 18 * *' # At 05:00 on day-of-month 18, the day development on a milestone begins.
      description: '[On 18th] gitlab-org/gitlab'
      variables:
        PROMPT_FOR_DOCUMENTATION_LABEL: 1
    - cron: '0 2 8,23 * *' # At 02:00 on day-of-month 8 and 23.
      description: '[On 8th and 23nd] gitlab-org/gitlab'
      variables:
        # report
        TRIAGE_MERGE_REQUESTS_NEEDING_ATTENTION: 1
    - cron: '0 14 13 * *' # At 14:00 on day-of-month 13.
      description: '[On 13th] gitlab-org/gitlab bugs release post triage'
      variables:
        # report
        BUG_RELEASE_POST_TRIAGE: 1
    - cron: '0 14 13 * *' # At 14:00 on day-of-month 13.
      description: '[On 13th] gitlab-org/gitlab performance issues triage'
      variables:
        # report
        PERFORMANCE_RELEASE_POST_TRIAGE: 1

gitlab-org/license-gitlab-com:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 6457868
  variants:
    - description: '[DAILY] gitlab-org/license-gitlab-com'
      variables:
        # hygiene
        TRIAGE_CLOSE_OLD_ISSUES: 1

gitlab-org/omnibus-gitlab:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 20699
  variants:
    - description: '[DAILY] gitlab-org/omnibus-gitlab'
      cron: '0 1 * * *' # At 01:00.
      variables:
        # hygiene
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_LABEL_SEEKING_COMMUNITY_CONTRIBUTIONS: 1
        TRIAGE_LABEL_VULNERABILITY: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1

gitlab-org/plan:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 13453461
  variants:
    - description: '[4th & 19th every month] gitlab-org/plan refinement cadence'
      cron: '0 4 4,19 * *' # At 04:00 on day-of-month 4 and 19.
      variables:
        # hygiene
        TRIAGE_PLAN_STAGE_BACKEND_FRONTEND_CHECK: 1
    - description: '[8th & 22nd every month] gitlab-org/plan group label check'
      cron: '0 4 8,22 * *' # At 04:00 on day-of-month 8 and 22.
      variables:
        TRIAGE_PLAN_STAGE_GROUP_LABEL_CHECK: 1
    - description: '[18th every month] gitlab-org/plan end of release refinement'
      cron: '0 4 18 * *' # At 04:00 on day-of-month 18.
      variables:
        TRIAGE_PLAN_STAGE_END_OF_RELEASE_REFINEMENT: 1
    - description: '[29th every month] gitlab-org/plan security issue check'
      cron: '0 4 29 * *' # At 04:00 on day-of-month 29.
      variables:
        TRIAGE_PLAN_STAGE_SECURITY_ISSUE_CHECK: 1
    - description: '[WEEKLY] gitlab-org/plan Weekly build board refinement'
      cron: '0 7 * * 1' # At 07:00 every monday
      variables:
        TRIAGE_PLAN_STAGE_BUILD_BOARD_REFINEMENT: 1

gitlab-org/gitlab-orchestrator:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 14378900
  variants:
    - description: '[DAILY] gitlab-org/gitlab-orchestrator'
      cron: '0 1 * * *' # At 01:00.
      variables:
        # hygiene
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1

gitlab-org/gitlab-shell:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 14022
  variants:
    - description: '[DAILY] gitlab-org/gitlab-shell'
      cron: '0 1 * * *' # At 01:00.
      variables:
        # hygiene
        TRIAGE_LABEL_MISSED_SLO: 1
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_LABEL_SEEKING_COMMUNITY_CONTRIBUTIONS: 1
        TRIAGE_LABEL_VULNERABILITY: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1
    - description: '[WEEKLY] gitlab-org/gitlab-shell'
      cron: '0 2 * * 1' # At 02:00 on Monday.
      variables:
        # report
        TRIAGE_TEAM_SUMMARY: 1
    - cron: '0 2 8,23 * *' # At 02:00 on day-of-month 8 and 23.
      description: '[On 8th and 23nd] gitlab-org/gitlab-shell'
      variables:
        TRIAGE_SOURCE_PATH: gitlab-org/gitlab-shell
        # report
        TRIAGE_MERGE_REQUESTS_NEEDING_ATTENTION: 1

gitlab-org/security/gitlab:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 15642544
  variants:
    - description: '[DAILY] gitlab-org/security/gitlab'
      cron: '0 1 * * *' # At 01:00.
      variables:
        HYGIENE_REMOVE_BOT_REVIEWER: 1

gitlab-org/security-products:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: groups
      TRIAGE_SOURCE_PATH: 2452873
  variants:
    - cron: '2 2 * * 1' # Weekly at 02:02 on Monday.
      description: '[WEEKLY] gitlab-org/security-products'
      variables:
        # report
        TRIAGE_SECURITY_PRODUCTS_COMMUNITY_MERGE_REQUESTS: 1

gitlab-com/support-forum:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 63904
  variants:
    - description: '[DAILY] gitlab-com/support-forum'
      variables:
        # report
        TRIAGE_SUPPORT_FORUM_AUTO_CLOSURE: 1
