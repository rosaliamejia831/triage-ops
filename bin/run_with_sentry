#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
require 'sentry-raven'
require_relative '../lib/tee_stream'

def enable_sentry?
  ENV['CI_PIPELINE_SOURCE'] == 'schedule'
end

Raven.configure do |config|
  # Avoid implicit enabling of `Sentry` based on the presence of `SENTRY_DSN` variable
  config.dsn = ENV['SENTRY_DSN_PRODUCTION'] if enable_sentry?
end

if enable_sentry?
  result = TeeStream.exec(ARGV)

  unless result.status.success?
    triage_ci_variables = {
      'CI_JOB_NAME' => ENV['CI_JOB_NAME'],
      'CI_JOB_URL' => ENV['CI_JOB_URL'],
      'TRIAGE_SOURCE_PATH' => ENV['TRIAGE_SOURCE_PATH'],
      'TRIAGE_SOURCE_TYPE' => ENV['TRIAGE_SOURCE_TYPE'],
      'TRIAGE_POLICY_FILE' => ENV['TRIAGE_POLICY_FILE'],
      'service' => 'scheduled'
    }

    Raven.tags_context(triage_ci_variables)
    Raven.capture_message(result.stderr.string)

    exit result.status.exitstatus
  end
else
  exec(*ARGV)
end
